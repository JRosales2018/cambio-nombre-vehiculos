<?php

function logResultado($array)
{
  $ln = [];
  foreach ($array as $value) $ln[] = $value;
  $ln = implode(' | ', $ln).PHP_EOL;
  $h = fopen('log.txt', 'a');
  fwrite($h, $ln);
  fclose($h);
}

function toAscii($str, $replace = array(), $delimiter = '-') {
    if (!empty($replace))
      $str = str_replace((array)$replace, ' ', $str);

    $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
    $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
    $clean = strtolower(trim($clean, '-'));
    $clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);

    return $clean;
  }

function base64url_encode($data) {
  return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
}

function base64url_decode($data) {
  return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT));
}

function esDocumentoIdentificacion($tipo, $valor)
{
  $valida = true;

  if ($tipo == 'DNI')
  {
    //Modificado por si viene con el prefijo ES
    $valor = str_replace("ES", "", $valor);

    $letra = substr($valor, -1);
    $numeros = substr($valor, 0, -1);
    if (substr("TRWAGMYFPDXBNJZSQVHLCKE", $numeros%23, 1) == $letra && strlen($letra) == 1 && strlen ($numeros) == 8) {}
    else $valida = false;
  }

  if ($tipo == 'CIF')
  {
    $cif = strtoupper($valor);

    $cifRegEx1 = '/^[ABEH][0-9]{8}$/i';
    $cifRegEx2 = '/^[KPQS][0-9]{7}[A-J]$/i';
    $cifRegEx3 = '/^[CDFGJLMNRUVW][0-9]{7}[0-9A-J]$/i';

    if (preg_match($cifRegEx1, $cif) || preg_match($cifRegEx2, $cif) || preg_match($cifRegEx3, $cif)) {
      $control = $cif[strlen($cif) - 1];
      $suma_A = 0;
      $suma_B = 0;

      for ($i = 1; $i < 8; $i++) {
        if ($i % 2 == 0) $suma_A += intval($cif[$i]);
          else {
            $t = (intval($cif[$i]) * 2);
            $p = 0;

            for ($j = 0; $j < strlen($t); $j++) {
                $p += substr($t, $j, 1);
            }
            $suma_B += $p;
            }
      }

      $suma_C = (intval($suma_A + $suma_B)) . "";
      $suma_D = (10 - intval($suma_C[strlen($suma_C) - 1])) % 10;

      $letras = "JABCDEFGHI";

      if ($control >= "0" && $control <= "9") $valida = ($control == $suma_D);
      else $valida = (strtoupper($control) == $letras[$suma_D]);
    }
    else $valida = false;
  }

  if ($tipo == 'NIE')
  {
    $nif = strtoupper($valor);

    $nifRegEx = '/^[0-9]{8}[A-Z]$/i';
    $nieRegEx = '/^[XYZ][0-9]{7}[A-Z]$/i';

    $letras = "TRWAGMYFPDXBNJZSQVHLCKE";

    if (preg_match($nifRegEx, $nif))
      $valida = ($letras[(substr($nif, 0, 8) % 23)] == $nif[8]);
    else if (preg_match($nieRegEx, $nif))
    {
      if ($nif[0] == "X") $nif[0] = "0";
      else if ($nif[0] == "Y") $nif[0] = "1";
      else if ($nif[0] == "Z") $nif[0] = "2";

      $valida = ($letras[(substr($nif, 0, 8) % 23)] == $nif[8]);
    }
    else $valida = false;
  }

  return $valida;
}

function reconoceDocumentoIdentificacion($valor)
{
  if (esDocumentoIdentificacion('CIF', $valor)) return 'CIF';
  if (esDocumentoIdentificacion('DNI', $valor) || esDocumentoIdentificacion('NIE', $valor))
  {
    $primeraLetra = strtoupper($valor[0]);
    if ('X' == $primeraLetra || 'Y' == $primeraLetra || 'Z' == $primeraLetra) return 'NIE';
    return 'DNI';
  }
  return false;
}
