<?php
define('URL_BASE', 'https://www.ayudatpymes.com/cambio-nombre-coche/formulario');
define('URL_OK', URL_BASE.'?pago=ok');
define('URL_KO', URL_BASE.'?pago=ko');

define('ENPRO', false);

if (ENPRO) {
	define('ISS', 'iss-momopocket-9');
	define('SUB', 'sub-AyudaTpymes');
	define('CLAVEPRV', file_get_contents(__DIR__."/clave-privada-real.pem"));
	define('CODCOMERCIO', '5g2y3x1jaQ');
	define('CODTPV', 'aucVXRN8mX');
} else {
	define('ISS', 'iss-momopocket-29');
	define('SUB', 'sub-AyudaTpymes');
	define('CLAVEPRV', file_get_contents(__DIR__."/clave-privada.pem"));
	define('CODCOMERCIO', 'kQMe9Drb2B');
	define('CODTPV', 'LSP3iUPdrt');
}

//email de AyudaT emisor
// define('EMAIL', 'transferencias@ayudatpymes.com'); // transferencias@ayudatpymes.com
//
// //email(s) para envío de info completa sobre pagos, separados por comas
// define('EMAILINFO', 'contratacionvehiculos@ayudatpymes.com'); // contratacionvehiculos@ayudatpymes.com
//
// // Aviso de cliente interesado
// define('EMAILPREPAGO', 'vehiculos@ayudatpymes.com'); // vehiculos

define('EMAIL', 'juanrosales@ayudat.es'); // transferencias@ayudatpymes.com

//email(s) para envío de info completa sobre pagos, separados por comas
define('EMAILINFO', 'juanrosales@ayudat.es'); // contratacionvehiculos@ayudatpymes.com

// Aviso de cliente interesado
define('EMAILPREPAGO', 'juanrosales@ayudat.es'); // vehiculos

$phpmailerIsSMTP = true;
$phpmailer = [
  'SMTPAuth' => false,
  'Host' => 'localhost',
  'Port' => 25,
  //$mail->Username = "yourname@yourdomain";
  //$mail->Password = "yourpassword";
];
