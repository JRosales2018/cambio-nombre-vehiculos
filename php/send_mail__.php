<?php
/**
 * Created by PhpStorm.
 * User: ayuda-t
 * Date: 3/10/17
 * Time: 13:14
 */

if(!isset($_SESSION))
    session_start();

mandarMail('vehiculos@ayudatpymes.com','//  Transferencia de Vehículos:','D./Dña.: ' . $_POST['nombre'] . ', con teléfono: '. $_POST['tel'] . ' y correo: '. $_POST['email'].'<br>Ha solicitado información sobre TRANSFERENCIA DE VEHICULOS');
if(isset($_POST['pixel']) && !isset($_SESSION['pixel'])) {
    $_SESSION['pixel']='1';
    header('Location: ../index.php?r=pixel');
//    echo "pixel";
}
else
    header('Location: ../index.php?r=enviado');
//echo "nopixel";

 function mandarMail($para = 'vehiculos@ayudatpymes.com', $titulo , $mensaje , $fromaddress = '') {
    $CorreoPagina   = 'noreply@'.$_SERVER['SERVER_NAME'];
    $titulo         = '=?UTF-8?B?'.base64_encode($titulo).'?=';                         // Titulo coja tildes y ñ

    if ($fromaddress == '') $fromaddress = '"Página Web" <'.$CorreoPagina.'>';

    $cabeceras = 'MIME-Version: 1.0'."\r\n".
        'Content-type: text/html; charset=utf-8'."\r\n".                       // Tipo de texto en el correo.
        'From: '.$fromaddress."\r\n".                                          // La direccion de correo desde donde supuestamente se envió
        'Reply-To: '.$fromaddress."\r\n".                                      // La direccion de correo a donde se responderá (cuando el recepto haga click en RESPONDER)
        'Return-Path: '.$fromaddress."\r\n".                                   // responder a...
        'Message-ID: <'.time().'-'.$CorreoPagina.'>'."\r\n".                   // anti-spam
        'X-Mailer: PHP/'.phpversion()."\r\n";                                  // información sobre el sistema de envio de correos, en este caso la version de PHP

    ini_set('sendmail_from',$fromaddress); // anti-spam

    // Miramos en que servidor estamos, para no enviar correos a los cliente y asesores probando.
    $serverEstamos = str_replace('www.', false, strtolower($_SERVER['SERVER_NAME'])); // dv.com, locahost
    if ( $serverEstamos == 'pre.mispapeles.es' || $serverEstamos == 'dev.mispapeles.es' ){
        $paraQuieniba   = $para;                                                            // A quien enviamos el correo.
        $para           = 'vehiculos@ayudatpymes.com';        // Metemos para quienes va a ir.
        $titulo        .= '//  Transferencia de Vehículos: '.$serverEstamos;                      // Meto en el titulo desde que servidor ha sido enviado.
        $mensaje       .= '<br><br>/// Este correo iba dirigido a: '.$paraQuieniba.' ///'; // Ponemos al final del mensaje a quien iba.
    }

    if ( mail($para, $titulo, $mensaje, $cabeceras ) ){
        ini_restore('sendmail_from');
        return TRUE;
    } else {
        ini_restore('sendmail_from');
        return FALSE;
    }

}