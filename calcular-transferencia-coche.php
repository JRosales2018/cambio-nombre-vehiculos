<?php

    // Conexión DDBB
    $mysqli = new mysqli('localhost', 'dbo752406563', 'Junio2010', 'db752406563');

    /*
    * Esta es la forma OO "oficial" de hacerlo,
    * AUNQUE $connect_error estaba averiado hasta PHP 5.2.9 y 5.3.0.
    */
    if ($mysqli->connect_error) {
        die('Error de Conexión (' . $mysqli->connect_errno . ') '
        . $mysqli->connect_error);
    }

    /*
    * Use esto en lugar de $connect_error si necesita asegurarse
    * de la compatibilidad con versiones de PHP anteriores a 5.2.9 y 5.3.0.
    */
    if (mysqli_connect_error()) {
        die('Error de Conexión (' . mysqli_connect_errno() . ') '
        . mysqli_connect_error());
    }

    // echo 'Éxito... ' . $mysqli->host_info . "\n";
    // echo '</br>';
?>

<?php
if(!isset($_SESSION))
    @session_start();
?>
    <!DOCTYPE html>
    <html lang="es">

    <head>
        <!-- Required meta tags -->
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="application-name" content="Calcular transferencia coche | Cambio nombre coche precio" />
        <meta name="author" content="AyudaT Pymes" />
        <meta name="title" content="Calcular transferencia coche | Cambio nombre coche precio">
        <meta name="dcterms.title" content="Calcular transferencia coche | Cambio nombre coche precio" />
        <meta name="description" content="¿Quieres saber cuánto cuesta cambiar de nombre un coche? Consulta aquí GRATIS el precio transferencia coche. Desde 24'99€, pago único." />
        <meta name="robots" content="index, follow" />
        <title>Calcular transferencia coche | Cambio nombre coche precio</title>

        <!-- Bootstrap CSS -->
        <!-- Latest compiled and minified CSS -->
        <link rel="icon" href="favicon.ico" type="image/x-icon"/>
        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon"/>

        <!-- MODALES -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/izimodal/1.5.1/css/iziModal.css">

        <!-- CSS LOCALES -->

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
     <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
     <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
         <script src="js/modernizr.custom.34982.js"></script>
        <script src="js/sketcher.js"></script>
        <script src="js/trigonometry.js"></script>
        <!-- FUENTES -->
        <link href="https://fonts.googleapis.com/css?family=Rajdhani:300,400,500,600,700" rel="stylesheet">
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
        <link rel="stylesheet" href="css/css.css">
        <link rel="stylesheet" href="css/animaciones.css">
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-8047470-2', 'auto');
            ga('send', 'pageview');

        </script>

        <!-- Facebook Pixel Code -->
        <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                document,'script','https://connect.facebook.net/en_US/fbevents.js');

            fbq('init', '660233234086506');
            fbq('track', 'PageView');

        </script>
        <noscript><img height='1' width='1' style='display:none'
                       src='https://www.facebook.com/tr?id=660233234086506&ev=PageView&noscript=1'
            /></noscript>
        <!-- End Facebook Pixel Code -->


        <style type="text/css">
            .error { padding-bottom: 15px; font-weight: bold; }
        </style>

    </head>

    <body>

        <div class="container-fluid">
            <div class="linea_gris"></div>
        </div>
        <div class="container-fluid max-1200">

            <!--row 1 menu superior-->
            <!-- -------------- -------------- -------------- -------------- -------------- -------------- -------------- -------------- -->

            <!--Medias Large-->

            <nav class="navbar navbar-light light-blue lighten-4">

    <!-- Navbar brand -->
    <a class="navbar-brand" href="https://www.ayudatpymes.com/cambio-nombre-coche">  <img src="Img/ayudate_pymes_logo.png" class="img-responsive" alt="Ayuda T Pymes">
    <span class="breadcrumbs"><span> | </span>TRANSFERENCIA VEHÍCULO</span></a>
    <!-- Collapse button -->
    <div class="content-nav">




        <a href="contratar">
    <span class="btn-contratar sacudir hvr-ripple-out-2-rojo contratar-nav">CONTRATA!</span>
    </a>

    

        <a href="http://www.ayudat.es/" target="_blank">
    <img src="Img/ayudat_pymes_icono_ayudat.png" alt="Ayuda T">
    </a>

    </div>



    <!-- Collapsible content -->

    <!-- Collapsible content -->

</nav>
<div class="row justify-content-around menu">
  <div class="link1"><a class="nav-link" href="contrato-compraventa-vehiculo">Generador de contratos</a></div>
  <div class="link1"><a class="nav-link" href="calcular-transferencia-coche">Calculadora de transferencia</a></div>
</div>


            <!--row 2 Franja roja con volante-->
            <!-- -------------- -------------- -------------- -------------- -------------- -------------- -------------- -------------- -->

            <!--Medias PCS-->


            <div class="row faldon_1 ">

                <div class="col-lg-12 col-md-12 text-center">
                    <center>
                        <h1 class="titulo ">Calcular transferencia coche</h1>
                        <img src="Img/precio-cambio-de-nombre-en-gestoria.png" class="img-responsive volante" alt="Precio cambio de nombre en gestoría">
                    </center>
                </div>
            </div>
</div>
<div class="container-fluid max-1200">
  <div class="row justify-content-center" id="calculadora">
<h2>Calcular transferencia de coche en varios clics.<br></h2>
<?php
if(isset($_GET['error'])){
  echo "<p class='texto-error' style='width:100%; text-align:center'>Lo sentimos. No se pudo encontrar una coincidencia. Inténtelo de nuevo.</p>";
  echo "<br>";
  echo "<a href='calcular-transferencia-coche'><button class='anterior' style='width:100%'>Anterior</button></a>";
  exit;
}
if(isset($_GET['paso'])){
  if($_GET['paso']=='1'){
    $marca = $_POST['marca'];

    $sql = "SELECT id, modelo FROM modelos WHERE marca = '$marca'";

    if (!$resultado = $mysqli->query($sql)) {
        // ¡Oh, no! La consulta falló.
        echo "Lo sentimos, este sitio web está experimentando problemas.";

        // De nuevo, no hacer esto en un sitio público, aunque nosotros mostraremos
        // cómo obtener información del error
        echo "Error: La ejecución de la consulta falló debido a: \n";
        echo "Query: " . $sql . "\n";
        echo "Errno: " . $mysqli->errno . "\n";
        echo "Error: " . $mysqli->error . "\n";
        exit;
    }

    // ¡Uf, lo conseguimos!. Sabemos que nuestra conexión a MySQL y nuestra consulta
    // tuvieron éxito, pero ¿tenemos un resultado?
    if ($resultado->num_rows === 0) {
        // ¡Oh, no ha filas! Unas veces es lo previsto, pero otras
        // no. Nosotros decidimos. En este caso, ¿podría haber sido
        // actor_id demasiado grande?
        echo "<p class='texto-error' style='width:100%; text-align:center'>Lo sentimos. No se pudo encontrar una coincidencia. Inténtelo de nuevo.</p>";
        echo "<br>";
        echo "<a href='calcular-transferencia-coche'><button class='anterior' style='width:100%'>Anterior</button></a>";
        exit;
    }

    // Ahora, sabemos que existe solamente un único resultado en este ejemplo, por lo
    // que vamos a colocarlo en un array asociativo donde las claves del mismo son los
    // nombres de las columnas de la tabla
    ?>
    <h3><span>Sólo necesitamos saber un par de cosas para<br> calcular el cambio de nombre de un coche:</span></h3>
    <div class="col-lg-4">
    <form method='post' action='calcular-transferencia-coche?paso=2' id="paso2">
    <label>Modelo</label>
    <select name="modelo">
            <option value="0">Seleccione el modelo</option>
    <?php
    while ($modelos = $resultado->fetch_assoc()) {
       echo '<option value="'.$modelos['id'].'">'.str_replace("?", "",utf8_decode($modelos['modelo'])).'</option>';
    }
    ?>
    </select>
    <label>Combustible</label>
    <select name="gd">
      <option value="0">Seleccione el tipo de combustible</option>
      <option value="G">Gasolina</option>
      <option value="D">Diesel</option>
      <option value="E">Eléctrico</option>
    </select>
    <label>Antigüedad</label>
    <select name="fecha">
      <option value="0">Seleccione la antigüedad</option>
      <option value="1">Hasta 1 año</option>
      <option value="2">Más de 1 año</option>
      <option value="3">Más de 2 años</option>
      <option value="4">Más de 3 años</option>
      <option value="5">Más de 4 años</option>
      <option value="6">Más de 5 años</option>
      <option value="7">Más de 6 años</option>
      <option value="8">Más de 7 años</option>
      <option value="9">Más de 8 años</option>
      <option value="10">Más de 9 años</option>
      <option value="11">Más de 10 años</option>
      <option value="12">Más de 11 años</option>
      <option value="13">Más de 12 años</option>
      <option value="14">Vehículo clásico (30 años o más)</option>
    </select>

    <input type="submit" value="Siguiente">
    </form>
    <a href="calcular-transferencia-coche"><button class="anterior">Anterior</button></a>
    <?php
    $resultado->free();
  }
  if($_GET['paso']=='2'){
    ?>
    <h3><span>¡Sólo algunos detalles más y terminamos con el<br> cálculo de cambio de nombre del vehículo!</span></h3>
    <div class="col-lg-3 formularioCalculadora">
    <form method='post' action='calc.php' id="paso3">
      <label>Perfil profesional</label>
      <select name="particular">
        <option value="0">Seleccione:</option>
        <option value="particular">Particular</option>
        <option value="sector">Profesional del sector</option>
        <option value="despacho">Despacho profesional</option>
      </select>
      <label>Comunidad <span>(en la que quieres realizar el trámite)</span></label>
      <select name="comunidad">
        <option value="0">Comunidad autónoma:</option>
        <option value="Andalucia">Andalucía</option>
        <option value="Aragon">Aragón</option>
        <option value="Asturias">Asturias</option>
        <option value="Baleares">Baleares</option>
        <option value="Canarias">Canarias</option>
        <option value="Cantabria">Cantabria</option>
        <option value="CastillaLaMancha">Castilla La Mancha</option>
        <option value="CastillaLeon">Castilla Leon</option>
        <option value="Cataluna">Cataluña</option>
        <option value="Ceuta">Ceuta</option>
        <option value="Valencia">Valencia</option>
        <option value="Extremadura">Extremadura</option>
        <option value="Galicia">Galicia</option>
        <option value="LaRioja">La Rioja</option>
        <option value="Madrid">Madrid</option>
        <option value="Melilla">Melilla</option>
        <option value="Murcia">Murcia</option>
        <option value="Navarra">Navarra</option>
        <option value="PaisVasco">País Vasco</option>
      </select>
      <input type="hidden" name="modelo" value="<?php echo $_POST['modelo'] ?>" />
      <input type="hidden" name="fecha" value="<?php echo $_POST['fecha'] ?>" />
      <input type="submit" value="Siguiente">
    </form>
    <a href="calcular-transferencia-coche"><button class="anterior">Anterior</button></a>
    <?php
  }

}else{

    $sql = "SELECT marca FROM modelos GROUP BY marca";

if (!$resultado = $mysqli->query($sql)) {
    // ¡Oh, no! La consulta falló.
    echo "Lo sentimos, este sitio web está experimentando problemas.";

    // De nuevo, no hacer esto en un sitio público, aunque nosotros mostraremos
    // cómo obtener información del error
    echo "Error: La ejecución de la consulta falló debido a: \n";
    echo "Query: " . $sql . "\n";
    echo "Errno: " . $mysqli->errno . "\n";
    echo "Error: " . $mysqli->error . "\n";
    exit;
}

// ¡Uf, lo conseguimos!. Sabemos que nuestra conexión a MySQL y nuestra consulta
// tuvieron éxito, pero ¿tenemos un resultado?
if ($resultado->num_rows === 0) {
    // ¡Oh, no ha filas! Unas veces es lo previsto, pero otras
    // no. Nosotros decidimos. En este caso, ¿podría haber sido
    // actor_id demasiado grande?
    echo "Lo sentimos. No se pudo encontrar una coincidencia. Inténtelo de nuevo.";
    exit;
}

// Ahora, sabemos que existe solamente un único resultado en este ejemplo, por lo
// que vamos a colocarlo en un array asociativo donde las claves del mismo son los
// nombres de las columnas de la tabla
?>
<h3><span>¿Cuál es la marca del coche <br>que se transfiere?</span></h3>
<div class="col-lg-3  formulariocalcular-transferencia-coche">
<form method='post' action='calcular-transferencia-coche?paso=1' id="paso1">
<label>Marca</label>
<select name="marca">
        <option value="0">Marca del vehículo</option>
<?php
while ($marcas = $resultado->fetch_assoc()) {
   echo '<option value="'.$marcas['marca'].'">'.$marcas['marca'].'</option>';
}
?>
</select>
<input type="submit" value="Siguiente">
</form>
<?php
// El script automáticamente liberará el resultado y cerrará la conexión
// a MySQL cuando finalice, aunque aquí lo vamos a hacer nostros mismos
$resultado->free();
}?>
</div>
</div>
</div>
<div class="row empresas justify-content-md-center">
 <a href="https://www.ayudatpymes.com"><div class="item-empresas item-empresas1"></div></a>
 <a href="https://ayudatlegal.com/"><div class="item-empresas item-empresas2"></div></a>
 <a href="https://www.ayudatpymes.com/para-despachos/" target="_blank"><div class="item-empresas item-empresas3"></div></a>
 <a href="http://ayudatlearning.com/" target="_blank"><div class="item-empresas item-empresas4"></div></a>
 <a href="https://www.ayudatpymes.com/programa-facturacion-gratuito/" target="_blank"><div class="item-empresas item-empresas5"></div></a>
 <a href="http://liquidoo.es/" target="_blank"><div class="item-empresas item-empresas6"></div></a>
 <a href="https://www.ayudatpymes.com/para-despachos/software-asesorias/" target="_blank"><div class="item-empresas item-empresas7"></div></a>
 <a href="http://ayudat.es/alma" target="_blank"><div class="item-empresas item-empresas8"></div></a>

</div>

<footer>
<div class="logo-footer"><img src="Img/logo-footer.jpg" alt="AyudaT Pymes"/></div>
<div class="row footer">

<div class="col-sm-12 col-lg-7">
    <div class="col-sm-4 col-lg-4 web-map">
        <a href="/">Inicio</a>
        <a href="servicios">Servicios</a>
        <a href="asesoria">Autónomo y empresas</a>
        <a href="alta-autonomos">Alta autónomo</a>
        <a href="crear-empresa">Crear empresa</a>
        <a href="abrir-sucursal">Abrir sucursal</a>
        <a href="emprendedores">Emprendedores</a>
        <a href="empresas-sociales">Empresas sociales</a>
        <a href="http://etlsport.es/" target="_blank">Asesoría para deportistas</a>
    </div>
    <div class="col-sm-4 col-lg-4 web-map">
        <a href="juridica">Asesoría jurídica</a>
        <a href="patentes-y-marcas">Registro de marcas</a>
        <a href="servicio-rgpd">RGPD</a>
        <a href="curso-online">Cursos de formación</a>
        <a href="https://www.ayudatpymes.com/cambio-nombre-coche" target="_blank">Transferencia de vehículos</a>
        <a href="https://www.ayudatpymes.com/programa-facturacion-gratuito/" target="_blank">Programa facturación Gratis</a>
        <a href="https://www.ayudatpymes.com/para-despachos/" target="_blank">Para despachos</a>
        <a href="http://gestron.es/" target="_blank">Blog GesTron</a>
        <a href="https://www.ayudatpymes.com/para-despachos/software-asesorias/" target="_blank">Software asesoría</a>

    </div>
    <div class="col-sm-3 col-lg-3 contact">
        <a href="asesoria-online-ayuda-t-pymes">Conócenos</a>
        <a href="contacto-asesoria-ayuda-t-pymes">Contacto</a>
        <a href="http://www.ayudat.es/" target="_blank">Ayuda T</a>
        <a href="https://mispapeles.es/registro/"><span class="float-right mar-right-16 radj-reg-11 button-nav grey">acceso cliente</span></a>
    </div>
</div>
<div class="col-sm-6 col-lg-3">

    <div class="col-sm-12 col-lg-12 contact-img">
        <p>Conectar</p>
        <div class="social">
            <a href="https://www.facebook.com/AyudaTPymes/" target="_blank"><img src="Img/fb_icon.png" alt="facebook" /></a>
            <a href="https://twitter.com/AyudaTPymes" target="_blank"><img src="Img/tw_icon.png" alt="tweeter"/></a>
            <a href="https://www.instagram.com/ayudatpymes/" target="_blank"><img src="Img/ins_icon.png" alt="instagram" /></a>
            <a href="https://www.linkedin.com/company/ayuda-t-pymes/" target="_blank"><img src="Img/lin_icon.png" alt="linkedin"/></a>
        </div>
        <p>¿Alguna duda laboral, fiscal o contable?</p>
        <a href="http://www.ayudapedia.es/" target="_blank"><img src="Img/ayudapedia.jpg" alt="Ayudapedia" /></a>
    </div>
</div>
<div class="col-sm-4 col-lg-2 footer-phone">
    <p class="footer-phone-num phone"><a href="tel:900100162">T. 900 100 162</a></p>
<p class="footer-phone-txt">Infórmate sin compromiso</p>

<p class="footer-phone-txt2">Atención al cliente</p>
    <p class="footer-phone-num2 phone"><a href="tel:856500776">T. 856 500 776</a></p>
<p class="footer-phone-txt3">clientes@ayudatpymes.com</p>

</div>
</div>
<div class="row legal justify-content-md-center">
<img src="Img/logo-footer-legal.jpg" alt="AyudaT"/>
<p class="text-legal1">Ayuda-T un lugar todas las Soluciones S.L. AYUDA-T PYMES © 2017 | <a href="terminos_y_condiciones" target="_blank">Términos y condiciones</a> | <a href="politica_de_privacidad" target="_blank">Política de privacidad</a> | <br class="visible-md"><a href="acuerdo_de_procesamiento_de_datos" target="_blank">Procesamiento de datos</a></p>
<p class="condiciones-legal">*Los precios que aparecen en la web no incluyen IVA</p>
</div>
</footer>



<!--//BLOQUE COOKIES-->
<div id="barraaceptacion" style="display: none;">
  <div class="inner">
      Solicitamos su permiso para obtener datos estadísticos de su navegación en esta web, en cumplimiento del Real Decreto-ley 13/2012. Si continúa navegando consideramos que acepta el uso de cookies.
      <a href="javascript:void(0);" class="ok" onclick="PonerCookie();"><b>OK</b></a> |
      <a href="https://www.ayudatpymes.com/servicios-rgpd" target="_blank" class="info">Más información</a>
  </div>
</div>

<?php
$pixel='0'; $envio = 0; $gracias='0';

if(isset($_GET['r'])) {
if($_GET['r']=='pixel'){
  $pixel='1'; $envio = 0; $gracias='0';
} else {
  $gracias = '1';$pixel='0'; $envio = 0;
}
}
?>

  <!--//FIN BLOQUE COOKIES-->

  <!--Formulario para MailRelay-->


  <!--fin mailrelay-->

  <!-- Latest compiled and minified JavaScript -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
          integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
          crossorigin="anonymous"></script>


  <!-- Moales -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/izimodal/1.5.1/js/iziModal.js"></script>


  <!-- Para las ALERTAS -->

  <script src="js/sweetalert.min.js"></script>

  <script>
      $(document).ready(function () {


      // proceso de control de datos introducidos correctamente

      $("#enviar").bind('click', function(){
          var nombre      = $("#nombre").val();
          var direccion   = $("#direccion").val();
          var email       = $("#email").val();
          var telefono    = $("#tel").val();

          if(nombre == "" || direccion == "" || email == "" || telefono == "" ){
              swal({
                  title: "Heeyyy",
                  text: "Debes introducir todos los datos",
                  icon: "error",
                  button: " Enterad@ ",
              });
              return;
          }
      });
      });
  //
  //    $(".formulario_contrata").bind('submit', function(){
  //        var email = $("#email").val();
  //        $.ajax({
  //            url: 'https://ayudatpymes.ip-zone.com/ccm/subscribe/index/form/g2d8ch5kzy',
  //            data: {'groups[]':'18', 'email':email},
  //            cache: false,
  //            type: 'POST'
  //        }).always(function( data ) {
  //
  //        });
  //    });




  </script>
  <script type="text/javascript">
      /* <![CDATA[ */
      var google_conversion_id = 1038674096;
      var google_custom_params = window.google_tag_params;
      var google_remarketing_only = true;
      /* ]]> */
  </script>
  <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
  </script>
  <noscript>
      <div style="display:inline;">
          <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1038674096/?guid=ON&amp;script=0"/>
      </div>
  </noscript>

  </body>
  </html>

<?php
$mysqli->close();
?>
