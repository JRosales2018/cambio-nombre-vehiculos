<?php

$ITP = $_GET['ITP'];
$perfil = $_GET['perfil'];
$comunidad = $_GET['comunidad'];
$honorarios= 0;
if($perfil == 'particular'){
$honorarios = 49.99;
}else if($perfil == 'sector'){
$honorarios = 24.99;
}else if($perfil == 'despacho'){
$honorarios = 34.99;
}
//Constantes
$iva = ($honorarios * 10)/100;
$trafico = 54.60;
$gestores = 6;
$correo = 4.5;
$total = $ITP + $honorarios + $iva + $trafico + $gestores + $correo;
if($comunidad == 'Ceuta' || $comunidad == 'Melilla'){
  $descuento = ($total * 50)/100;
  $total = $total - $descuento;
}


?>

<!DOCTYPE html>
<html lang="es">
<head>
    <!-- Required meta tags -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <!-- Latest compiled and minified CSS -->

    <link rel="icon" href="favicon.ico" type="image/x-icon"/>
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon"/>

    <!-- MODALES -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/izimodal/1.5.1/css/iziModal.css">
    <meta name="robots" content="noindex, nofollow">
    <!-- CSS LOCALES -->

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
     <script src="js/modernizr.custom.34982.js"></script>
    <script src="js/sketcher.js"></script>
    <script src="js/trigonometry.js"></script>
    <!-- FUENTES -->
    <link href="https://fonts.googleapis.com/css?family=Rajdhani:300,400,500,600,700" rel="stylesheet">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/css.css">
    <link rel="stylesheet" href="css/animaciones.css">
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-8047470-2', 'auto');
        ga('send', 'pageview');

    </script>

    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
            n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
            document,'script','https://connect.facebook.net/en_US/fbevents.js');

        fbq('init', '660233234086506');
        fbq('track', 'PageView');

    </script>
    <noscript><img height='1' width='1' style='display:none'
                   src='https://www.facebook.com/tr?id=660233234086506&ev=PageView&noscript=1'
        /></noscript>
    <!-- End Facebook Pixel Code -->


    <style type="text/css">
        .error { padding-bottom: 15px; font-weight: bold; }
    </style>



</head>


    <body>

        <div class="container-fluid">
            <div class="linea_gris"></div>
        </div>



      <div class="container-fluid max-1200">
            <!--row 1 menu superior-->
            <!-- -------------- -------------- -------------- -------------- -------------- -------------- -------------- -------------- -->

            <!--Medias Large-->
            <nav class="navbar navbar-light light-blue lighten-4">

    <!-- Navbar brand -->
    <a class="navbar-brand" href="https://www.ayudatpymes.com/cambio-nombre-coche">  <img src="Img/ayudate_pymes_logo.png" class="img-responsive" alt="Ayuda T Pymes">
    <span class="breadcrumbs"><span> | </span>TRANSFERENCIA VEHÍCULO</span></a>
    <!-- Collapse button -->
    <div class="content-nav">




        <a href="contratar">
    <span class="btn-contratar sacudir hvr-ripple-out-2-rojo contratar-nav">CONTRATA!</span>
    </a>



        <a href="http://www.ayudat.es/" target="_blank">
    <img src="Img/ayudat_pymes_icono_ayudat.png" alt="Ayuda T">
    </a>

    </div>



    <!-- Collapsible content -->

    <!-- Collapsible content -->

</nav>
<div class="row justify-content-around menu">
  <div class="link1"><a class="nav-link" href="contrato-compraventa-vehiculo">Generador de contratos</a></div>
  <div class="link1"><a class="nav-link" href="calcular-transferencia-coche">Calculadora de transferencia</a></div>
</div>
<div class="row margen-up-20 faldon_1 ">

    <div class="col-lg-12 col-md-12 text-center">
        <center>
            <h1 class="titulo">Calculadora de transferencia de vehículos</h1>
            <img src="Img/precio-cambio-de-nombre-en-gestoria.png" class="img-responsive volante" alt="Precio cambio de nombre en gestoría">
        </center>
    </div>
</div>
</div>
<div class="container-fluid max-1200">
<div class="row justify-content-center" id="resultados">
  <h2>GESTIÓN TRASFERENCIA DE VEHÍCULOS</h2>
  <div class="col-lg-5">

    <div class="honorarios row">
      <div class="col-lg-6 titulo-resultado">
        <p>HONORARIOS TRANSFERENCIA <br>DE VEHÍCULOS</p>
      </div>
      <div class="col-lg-6 subtotal-resultado">
        <p>Subtotal <span><?php echo number_format($honorarios, 2, ",", "."); ?> €</span></p>
      </div>
    </div>

    <div class="itp row">
      <div class="col-lg-6 titulo-resultado">
        <p>IMPUESTO DE TRANSMISIONES PATRIMONIALES (ITP)</p>
      </div>
      <div class="col-lg-6 subtotal-resultado">
        <p>Subtotal <span><?php echo number_format($ITP, 2, ",", ".") ?> €</span></p>
      </div>
    </div>

    <div class="trafico row">
      <div class="col-lg-6 titulo-resultado">
        <p>TASAS DE TRÁFICO</p>
      </div>
      <div class="col-lg-6 subtotal-resultado">
        <p>Subtotal <span><?php echo number_format($trafico, 2, ",", ".") ?> €</span></p>
      </div>
    </div>

    <div class="gestores row">
      <div class="col-lg-6 titulo-resultado">
        <p>COLEGIO DE GESTORES (PDF)</p>
      </div>
      <div class="col-lg-6 subtotal-resultado">
        <p>Subtotal <span><?php echo number_format($gestores, 2, ",", ".") ?> €</span></p>
      </div>
    </div>

    <div class="gestores row">
      <div class="col-lg-6 titulo-resultado">
        <p>CORREO CERTIFICADO</p>
      </div>
      <div class="col-lg-6 subtotal-resultado">
        <p>Subtotal <span><?php echo number_format($correo, 2, ",", ".") ?> €</span></p>
      </div>
    </div>
<?php if($comunidad == 'Ceuta' || $comunidad == 'Melilla'): ?>
  <div class="descuento row">
    <div class="col-lg-6 titulo-resultado">
      <p>RESIDENTE EN <?php echo $comunidad ?> (-50%)</p>
    </div>
    <div class="col-lg-6 subtotal-resultado">
      <p><span>-<?php
      echo number_format($descuento, 2, ",", ".") ?> €</span></p>
    </div>
  </div>
<?php endif ?>

    <div class="linea row"></div>
    <div class="total row">
      <div class="col-lg-6 titulo-resultado">
      </div>
      <div class="col-lg-6 subtotal-resultado">
        <p>TOTAL <span><?php echo number_format($total, 2, ",", ".") ?> €</span></p>
      </div>
    </div>

    <a href="contratar"><button class="contratar-total">CONTRATAR</button</a>

  </div>
</div>
</div>

<div class="row empresas justify-content-md-center">
 <a href="https://www.ayudatpymes.com"><div class="item-empresas item-empresas1"></div></a>
 <a href="https://ayudatlegal.com/"><div class="item-empresas item-empresas2"></div></a>
 <a href="https://www.ayudatpymes.com/para-despachos/" target="_blank"><div class="item-empresas item-empresas3"></div></a>
 <a href="http://ayudatlearning.com/" target="_blank"><div class="item-empresas item-empresas4"></div></a>
 <a href="https://www.ayudatpymes.com/programa-facturacion-gratuito/" target="_blank"><div class="item-empresas item-empresas5"></div></a>
 <a href="http://liquidoo.es/" target="_blank"><div class="item-empresas item-empresas6"></div></a>
 <a href="https://www.ayudatpymes.com/para-despachos/software-asesorias/" target="_blank"><div class="item-empresas item-empresas7"></div></a>
 <a href="http://ayudat.es/alma" target="_blank"><div class="item-empresas item-empresas8"></div></a>

</div>

<footer>
<div class="logo-footer"><img src="Img/logo-footer.jpg" alt="AyudaT Pymes"/></div>
<div class="row footer">

<div class="col-sm-12 col-lg-7">
    <div class="col-sm-4 col-lg-4 web-map">
        <a href="/">Inicio</a>
        <a href="servicios">Servicios</a>
        <a href="asesoria">Autónomo y empresas</a>
        <a href="alta-autonomos">Alta autónomo</a>
        <a href="crear-empresa">Crear empresa</a>
        <a href="abrir-sucursal">Abrir sucursal</a>
        <a href="emprendedores">Emprendedores</a>
        <a href="empresas-sociales">Empresas sociales</a>
        <a href="http://etlsport.es/" target="_blank">Asesoría para deportistas</a>
    </div>
    <div class="col-sm-4 col-lg-4 web-map">
        <a href="juridica">Asesoría jurídica</a>
        <a href="patentes-y-marcas">Registro de marcas</a>
        <a href="servicio-rgpd">RGPD</a>
        <a href="curso-online">Cursos de formación</a>
        <a href="https://www.ayudatpymes.com/cambio-nombre-coche/" target="_blank">Transferencia de vehículos</a>
        <a href="https://www.ayudatpymes.com/programa-facturacion-gratuito/" target="_blank">Programa facturación Gratis</a>
        <a href="https://www.ayudatpymes.com/para-despachos/" target="_blank">Para despachos</a>
        <a href="http://gestron.es/" target="_blank">Blog GesTron</a>
        <a href="https://www.ayudatpymes.com/para-despachos/software-asesorias/" target="_blank">Software asesoría</a>

    </div>
    <div class="col-sm-3 col-lg-3 contact">
        <a href="asesoria-online-ayuda-t-pymes">Conócenos</a>
        <a href="contacto-asesoria-ayuda-t-pymes">Contacto</a>
        <a href="http://www.ayudat.es/" target="_blank">Ayuda T</a>
        <a href="https://mispapeles.es/registro/"><span class="float-right mar-right-16 radj-reg-11 button-nav grey">acceso cliente</span></a>
    </div>
</div>
<div class="col-sm-6 col-lg-3">

    <div class="col-sm-12 col-lg-12 contact-img">
        <p>Conectar</p>
        <div class="social">
            <a href="https://www.facebook.com/AyudaTPymes/" target="_blank"><img src="Img/fb_icon.png" alt="facebook" /></a>
            <a href="https://twitter.com/AyudaTPymes" target="_blank"><img src="Img/tw_icon.png" alt="tweeter"/></a>
            <a href="https://www.instagram.com/ayudatpymes/" target="_blank"><img src="Img/ins_icon.png" alt="instagram" /></a>
            <a href="https://www.linkedin.com/company/ayuda-t-pymes/" target="_blank"><img src="Img/lin_icon.png" alt="linkedin"/></a>
        </div>
        <p>¿Alguna duda laboral, fiscal o contable?</p>
        <a href="http://www.ayudapedia.es/" target="_blank"><img src="Img/ayudapedia.jpg" alt="Ayudapedia" /></a>
    </div>
</div>
<div class="col-sm-4 col-lg-2 footer-phone">
    <p class="footer-phone-num phone"><a href="tel:900100162">T. 900 100 162</a></p>
<p class="footer-phone-txt">Infórmate sin compromiso</p>

<p class="footer-phone-txt2">Atención al cliente</p>
    <p class="footer-phone-num2 phone"><a href="tel:856500776">T. 856 500 776</a></p>
<p class="footer-phone-txt3">clientes@ayudatpymes.com</p>

</div>
</div>
<div class="row legal justify-content-md-center">
<img src="Img/logo-footer-legal.jpg" alt="AyudaT"/>
<p class="text-legal1">Ayuda-T un lugar todas las Soluciones S.L. AYUDA-T PYMES © 2017 | <a href="terminos_y_condiciones" target="_blank">Términos y condiciones</a> | <a href="politica_de_privacidad" target="_blank">Política de privacidad</a> | <br class="visible-md"><a href="acuerdo_de_procesamiento_de_datos" target="_blank">Procesamiento de datos</a></p>

</div>
</footer>



<!--//BLOQUE COOKIES-->
<div id="barraaceptacion" style="display: none;">
  <div class="inner">
      Solicitamos su permiso para obtener datos estadísticos de su navegación en esta web, en cumplimiento del Real Decreto-ley 13/2012. Si continúa navegando consideramos que acepta el uso de cookies.
      <a href="javascript:void(0);" class="ok" onclick="PonerCookie();"><b>OK</b></a> |
      <a href="https://www.ayudatpymes.com/servicios-rgpd" target="_blank" class="info">Más información</a>
  </div>
</div>

<?php
$pixel='0'; $envio = 0; $gracias='0';

if(isset($_GET['r'])) {
if($_GET['r']=='pixel'){
  $pixel='1'; $envio = 0; $gracias='0';
} else {
  $gracias = '1';$pixel='0'; $envio = 0;
}
}
?>

  <!--//FIN BLOQUE COOKIES-->

  <!--Formulario para MailRelay-->


  <!--fin mailrelay-->

  <!-- Latest compiled and minified JavaScript -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
          integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
          crossorigin="anonymous"></script>


  <!-- Moales -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/izimodal/1.5.1/js/iziModal.js"></script>


  <!-- Para las ALERTAS -->

  <script src="js/sweetalert.min.js"></script>

  <script>
      $(document).ready(function () {


      // proceso de control de datos introducidos correctamente

      $("#enviar").bind('click', function(){
          var nombre      = $("#nombre").val();
          var direccion   = $("#direccion").val();
          var email       = $("#email").val();
          var telefono    = $("#tel").val();

          if(nombre == "" || direccion == "" || email == "" || telefono == "" ){
              swal({
                  title: "Heeyyy",
                  text: "Debes introducir todos los datos",
                  icon: "error",
                  button: " Enterad@ ",
              });
              return;
          }
      });
      });
  //
  //    $(".formulario_contrata").bind('submit', function(){
  //        var email = $("#email").val();
  //        $.ajax({
  //            url: 'https://ayudatpymes.ip-zone.com/ccm/subscribe/index/form/g2d8ch5kzy',
  //            data: {'groups[]':'18', 'email':email},
  //            cache: false,
  //            type: 'POST'
  //        }).always(function( data ) {
  //
  //        });
  //    });




  </script>
  <script type="text/javascript">
      /* <![CDATA[ */
      var google_conversion_id = 1038674096;
      var google_custom_params = window.google_tag_params;
      var google_remarketing_only = true;
      /* ]]> */
  </script>
  <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
  </script>
  <noscript>
      <div style="display:inline;">
          <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1038674096/?guid=ON&amp;script=0"/>
      </div>
  </noscript>

  </body>
  </html>
