$(function() {
  $('.item1').click(function(){
   $('#contrata-1').css('display', 'none');
   $('#comprador').css('display', 'block');
  });
  $('#anterior1').click(function(){
    $('#comprador').css('display', 'none');
    $('#contrata-1').css('display', 'block');
  });

  $('.item2').click(function(){
   $('#comprador').css('display', 'none');
   $('#persona').css('display', 'block');
  });
  $('#anterior2').click(function(){
    $('#persona').css('display', 'none');
    $('#comprador').css('display', 'block');
  });

  $('.item3').click(function(){
    if($('#particular').is(':checked')){
      $('#enviar0').html('PAGAR');
      $('#titulo-enviar').html('Deja tus datos ahí abajo y empezamos cuando quieras:');
    }else{
      $('#enviar0').html('INFÓRMATE');
      $('#titulo-enviar').html('Deja tus datos ahí abajo, te llamo y empezamos cuando quieras:');
    }
   $('#persona').css('display', 'none');
   $('#datos-contratar').css('display', 'block');
  });
  $('#anterior3').click(function(){
    $('#datos-contratar').css('display', 'none');
    $('#persona').css('display', 'block');
  });

  $('#siguiente3').mouseover(function(){

  });
});
