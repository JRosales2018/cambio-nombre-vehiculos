<?php
if(!isset($_SESSION))
    @session_start();
?>
    <!DOCTYPE html>
    <html lang="es">

    <head>
        <!-- Required meta tags -->
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="application-name" content="Tranferencia vehículo desde 24,99€" />
        <meta name="author" content="AyudaT Pymes" />
        <meta name="title" content="Tranferencia vehículo desde 24,99€">
        <meta name="dcterms.title" content="Tranferencia vehículo desde 24,99€" />
        <meta name="description" content="Gestionamos transferencia vehículo para particulares y profesionales: concesionarios, gestorías, compra-ventas, talleres, etc." />
        <meta name="robots" content="index, follow" />
        <title>Tranferencia vehículo desde 24,99€</title>

        <!-- Bootstrap CSS -->
        <!-- Latest compiled and minified CSS -->
        <link rel="icon" href="favicon.ico" type="image/x-icon"/>
        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon"/>

        <!-- MODALES -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/izimodal/1.5.1/css/iziModal.css">

        <!-- CSS LOCALES -->

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
     <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
     <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
         <script src="js/modernizr.custom.34982.js"></script>
        <script src="js/sketcher.js"></script>
        <script src="js/trigonometry.js"></script>
        <!-- FUENTES -->
        <link href="https://fonts.googleapis.com/css?family=Rajdhani:300,400,500,600,700" rel="stylesheet">
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
        <link rel="stylesheet" href="css/css.css">
        <link rel="stylesheet" href="css/animaciones.css">
        <!-- Facebook Pixel Code -->
        <?php if(isset($_GET['gracias']) || isset($_GET['pago']) || isset($_GET['r'])){
    ?>
      <script>
      !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
      n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
      n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
      t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
      document,'script','https://connect.facebook.net/en_US/fbevents.js');

      fbq('init', '660233234086506');
      fbq('track', 'PageView');
        fbq('track', 'CompleteRegistration');
      </script>
        <noscript><img height='1' width='1' style='display:none'
      src='https://www.facebook.com/tr?id=660233234086506&ev=PageView&noscript=1'
      /></noscript>
          <script data-obct type="text/javascript">
    /* DO NOT MODIFY THIS CODE */
    !function(_window, _document) {
    var OB_ADV_ID='004195809e89cb3dd64a935b5724ee9f17';
    if (_window.obApi) {var toArray = function(object) {return Object.prototype.toString.call(object) === '[object Array]' ? object : [object];};_window.obApi.marketerId = toArray(_window.obApi.marketerId).concat(toArray(OB_ADV_ID));return;}
    var api = _window.obApi = function() {api.dispatch ? api.dispatch.apply(api, arguments) : api.queue.push(arguments);};api.version = '1.1';api.loaded = true;api.marketerId = OB_ADV_ID;api.queue = [];var tag = _document.createElement('script');
    tag.async = true;tag.src = '//amplify.outbrain.com/cp/obtp.js';tag.type = 'text/javascript';var script = _document.getElementsByTagName('script')[0];script.parentNode.insertBefore(tag, script);}(window, document);
    obApi('track', 'PAGE_VIEW');
    </script>
        <?php } else{?>

        <script>
      !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
      n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
      n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
      t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
      document,'script','https://connect.facebook.net/en_US/fbevents.js');

      fbq('init', '660233234086506');
      fbq('track', 'PageView');
      </script>
      <noscript><img height='1' width='1' style='display:none'
      src='https://www.facebook.com/tr?id=660233234086506&ev=PageView&noscript=1'
      /></noscript>
      <!-- End Facebook Pixel Code -->
    <?php } ?>

        <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-8047470-2"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-8047470-2');
    </script>


        <style type="text/css">
            .error { padding-bottom: 15px; font-weight: bold; }
        </style>

    </head>

    <body>

        <div class="container-fluid">
            <div class="linea_gris"></div>
        </div>
        <div class="container-fluid max-1200">

            <!--row 1 menu superior-->
            <!-- -------------- -------------- -------------- -------------- -------------- -------------- -------------- -------------- -->

            <!--Medias Large-->

            <nav class="navbar navbar-light light-blue lighten-4">

    <!-- Navbar brand -->
    <a class="navbar-brand" href="https://www.ayudatpymes.com/cambio-nombre-coche">  <img src="Img/ayudate_pymes_logo.png" class="img-responsive" alt="Ayuda T Pymes">
    <span class="breadcrumbs"><span> | </span>TRANSFERENCIA VEHÍCULO</span></a>
    <!-- Collapse button -->
    <div class="content-nav">




        <a href="contratar">
    <span class="btn-contratar sacudir hvr-ripple-out-2-rojo contratar-nav">CONTRATA!</span>
    </a>



        <a href="http://www.ayudat.es/" target="_blank">
    <img src="Img/ayudat_pymes_icono_ayudat.png" alt="Ayuda T">
    </a>

    </div>



    <!-- Collapsible content -->

    <!-- Collapsible content -->

</nav>
<div class="row justify-content-around menu">
  <div class="link1"><a class="nav-link" href="contrato-compraventa-vehiculo">Generador de contratos</a></div>
  <div class="link1"><a class="nav-link" href="calcular-transferencia-coche">Calculadora de transferencia</a></div>
</div>


            <!--row 2 Franja roja con volante-->
            <!-- -------------- -------------- -------------- -------------- -------------- -------------- -------------- -------------- -->

            <!--Medias PCS-->


            <div class="row faldon_1 ">

                <div class="col-lg-12 col-md-12 text-center">
                    <center>
                        <h1 class="titulo">TRANSFERENCIA VEHÍCULO <span class="online">ON LINE</span></h1>
                        <img src="Img/precio-cambio-de-nombre-en-gestoria.png" class="img-responsive volante" alt="Precio cambio de nombre en gestoría">
                    </center>
                </div>
            </div>



            <!--row 3 chica auto, precio, teléfono-->
            <!-- -------------- -------------- -------------- -------------- -------------- -------------- -------------- -------------- -->

            <div class="row text-center hidden-xs hidden-sm">

                <div class="col-lg-3 col-md-3">
                    <img src="Img/​​transferencia-de-vehiculo.png" class="faldon_izquierdo" alt="transferencia de vehículo">

                </div>

                <div class="col-lg-2 col-md-2">
                    <br>
                    <p class="semi-bold-24 gris_lp linea_media"><span class="espaciado-2">EMPIEZA A USAR</span></p>
                    <p class="semi-bold-24 gris_lp linea_media"><span class="espaciado-1" style="margin-left:-25px">TU COCHE NUEVO,</span>
                    </p>
                    <p class="semi-bold-25 cian_lp linea_media"><span class="espaciado-0" style="margin-left:-110px">EL PAPELEO DE LA</span></p>
                    <p class="semi-bold-25 cian_lp linea_media"><span class="espaciado-0" style="margin-left:-125px">TRANSFERENCIA DEL</span></p>
                    <p class="semi-bold-25 cian_lp linea_media"><span class="espaciado-0" style="margin-left:-265px">VEHÍCULO,</span></p>
                    <p class="semi-bold-25 cian_lp linea_media"><span class="espaciado-0" style="margin-left:-225px">​​ES COSA NUESTRA</span></p>
                </div>


                <div class="col-lg-2 col-md-2">
                    <center>
                        <img src="Img/ayudate_pymes_play.png" class=" margen-up-120" alt="flecha">
                        <br>

                    </center>
                </div>



                <div class="col-lg-3 col-md-4 text-center">

                        <p class="text_1 margen-up-10">DESDE <span class="rojo_lp">24,99 €</span></p>

                        <a href="contratar">
                    <span class="btn-contratar sacudir hvr-ripple-out-2-rojo" style="width:100%; padding:10px">CONTRATA!</span>
                </a>
                <span class="btn-info sacudir hvr-ripple-out-3-rojo open" style="width:100%; margin-top: 30px; padding:10px">MÁS iNFO!</span>

                </div>


                <div class="col-lg-2">

                </div>


            </div>







            <!--row 4banner cian-->
            <!-- -------------- -------------- -------------- -------------- -------------- -------------- -------------- -------------- -->
        </div>


        <div class="container-fluid banner visible-lg">
            <div class="row v-a-center">
                <div class="col-lg-12 col-md-12 text-center v-a-center">
                    <p class="text_3 margen-up-20">¡No sólo para particulares!
​​¿ERES PROFESIONAL?<br> Transferencia vehículo para talleres, compra-ventas, concesionarios, asesorías, gestorías...</p>
                    <span class="text_4 margen-up-10">¡HASTA 50% DE DESCUENTO!</span>
                    <br>
                </div>
            </div>
        </div>


        <div class="container-fluid max-1200">

            <!--row 5 Boton contratar hasta cara sonriente-->
            <!-- -------------- -------------- -------------- -------------- -------------- -------------- -------------- -------------- -->

            <div class="row hidden-xs" style="margin-top:10px">

                <div class="col-lg-1 col-md-1 col-sm-1 hidden-xs">

                </div>

                <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 text-center">
                    <center>

                        <br><br>
                        <h2 class="semi-bold-18 gris_lp espaciado-1">¿Vas a vender o comprar un coche?</h2>
                        <br>
                        <h4 class="semi-bold-18 gris_lp espaciado-1">¿Necesitas poner a tu nombre el vehículo que inscribiste a nombre de un familiar<br> para ahorrar en el seguro?</h4>


                        <h2 class="bold-29 rojo_lp espaciado-0 margen-up-20">TRANSFERENCIA VEHÍCULO SIN MOVERTE DE CASA
                        </h2>
                        <p class="bold-25 gris_lp espaciado-1  margen-up-20">DANOS UN TOQUE E INFÓRMATE SIN COMPROMISO</p>
                        <span class="span-telefono_grande margen-up-20 llama_ahora">900 100 162</span>
                        <br>
                        <img src="Img/gestoria-cambio-de-nombre-coche.png" style="max-height:40px" id="sonrie" class="sonrie" alt="Gestoría cambio de nombre coche">
                    </center>
                </div>

                <div class="col-lg-1 col-md-1 col-sm-1 hidden-xs">

                </div>
            </div>




            <!--row 5 Comparando -->
            <!-- -------------- -------------- -------------- -------------- -------------- -------------- -------------- -------------- -->

            <div class="row margen-up-20 hidden-xs">
                <div class="col-lg-2 col-sm-1 col-md-2"></div>

                <div class="col-lg-4 col-sm-5 col-md-4 col-xs-12 text-center">
                    <span class="bold-18 espaciado-0 gris_lp margen-up-10">¿Comparando precios para saber<br> cuánto cuesta la transferencia de un<br> coche o moto?</span>

                </div>
                <div class="col-lg-4 col-sm-5 col-md-4 col-xs-12 text-center">
                    <span class="bold-18 espaciado-0 gris_lp margen-up-10">Ahorra no sólo en el precio de la<br> transferencia de vehículos,<br><span
                    class="rojo_lp espaciado-0"> sino en la tranquilidad de no tener<br> que hacer NADA.</span></span>
                </div>
                <div class="col-lg-2 col-sm-1 col-md-2"></div>
            </div>




            <!--row 6 Todo Hecho -->
            <!-- -------------- -------------- -------------- -------------- -------------- -------------- -------------- -------------- -->

            <div class="container max-1200 triangulo_lateral visible-lg">

                <div class="row margen-up-50">
                    <div class="row">
                        <div class="col-lg-1 col-sm-1 col-md-1">

                        </div>

                        <div class="col-lg-6 col-md-7">
                            <br>
                            <img src="Img/ayudate_pymes_automovil_1x.png" class="saltos" alt="Cambio nombre coche">
                            <br><br><br><br>
                            <span class="text_5 rojo_lp espaciado-1 margen-up-20">​​TODO HECHO, TRANSFERENCIA COCHE EN BANDEJA</span>
                            <br>
                            <span class="text_1 espaciado-0">Cambio de nombre coche por <span class="precio_grande rojo_lp">24,99 </span><span class="rojo_lp">€</span><br> Pago único. IVA incluido</span>
                            <div style="margin-left:-40px;margin-top:20px">
                                <ul class="semi-bold-18 gris_lp espaciado-1 linea_media">
                                    <li>
                                        <h3 class="semi-bold-18 gris_lp espaciado-1 linea_media">Todos los trámites</h3>
                                    </li>
                                    <li>
                                        <h3 class="semi-bold-18 gris_lp espaciado-1 linea_media">Transferencia vehículo en el Registro Oficial de la DGT</h3>
                                    </li>
                                    <li>
                                        <h3 class="semi-bold-18 gris_lp espaciado-1 linea_media">Garantía profesional</h3>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-2" style="vertical-align: bottom">
                            <img src="Img/​​dgt-cambio-de-nombre-coche.png" id="dgt" style="position:absolute; margin-top:220px;left:-55%; max-height: 80px" class="" alt="DGT cambio de nombre coche">
                        </div>

                        <div class="col-lg-3 col-md-3 text-center">
                            <img src="Img/​​transferencia-coche.png" class="faldon_derecho" alt="​​Transferencia Coche">
                        </div>
                    </div>
                </div>
            </div>
            <div class="container hidden-md visible-md">

                <div class="row margen-up-50">
                    <div class="col-xs-12 col-sm-12">
                        <center>
                            <br>
                            <img src="Img/ayudate_pymes_automovil_1x.png" style="position: relative" class="saltos" alt="Cambio nombre vehículo">
                            <br><br>
                            <span class="text_5_xs rojo_lp espaciado-1 margen-up-10">​​TODO HECHO, TRANSFERENCIA COCHE EN BANDEJA</span>
                            <br><br>
                            <span class="bold-30_xs espaciado-0 gris_lp">Cambio de nombre coche por <br><span
                            class="precio_grande_xs rojo_lp">24,99</span><span class="rojo_lp">€</span><br> Pago único. IVA incluido</span>
                        </center>
                        <br>
                        <ul class="semi-bold-18 gris_lp espaciado-1 linea_alta">
                            <li><span class="li_xs">Todos los trámites</span></li>
                            <br>
                            <li><span class="li_xs">Transferencia vehículo en el Registro Oficial de la DGT</span></li>
                            <br>
                            <li><span class="li_xs">Garantía profesional</span></li>
                        </ul>

                    </div>

                </div>
            </div>


            <!--row 7 acelerometro-->
            <!-- -------------- -------------- -------------- -------------- -------------- -------------- -------------- -------------- -->

            <div class="row margen-up-50 hidden-xs hidden-sm">

                <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 text-center">
                    <br>
                    <div id="container">
                        <img src="Img/anim1.png" class="animated" alt="anim1">
                        <img src="Img/anim2.png" class="animated" alt="anim2">
                        <img src="Img/anim3.png" class="animated" alt="anim3">
                        <img src="Img/anim4.png" class="animated" alt="anim4">
                        <img src="Img/anim5.png" class="animated" alt="anim5">
                        <img src="Img/anim6.png" class="animated" alt="anim6">
                        <img src="Img/anim7.png" class="animated" alt="anim7">
                        <img src="Img/anim8.png" class="animated" alt="anim8">
                        <br>


                    </div>
                    <br><br>
                    <a href="contratar">
                <span id="btn-contratar_3" class="hvr-ripple-out-2-rojo btn-contratar_3 sacudir">CONTRATAR!</span>
            </a>
                </div>
            </div>


        </div>


        <!--row 8  Franja roja-->
        <!-- -------------- -------------- -------------- -------------- -------------- -------------- -------------- -------------- -->


        <div class="container-fluid bg_rojo_lp hidden-xs" style="padding-left:0px; padding-right: 0px; margin-top:80px">
            <div class="text-center">
                <div class="container-fluid bg_rojo_lp_activo">
                    <br>
                    <span id="duda" class="semi-bold-40 espaciado-2 blanco_lp">Todavía tienes alguna duda?</span>
                    <br>
                </div>
                <div class="row">

                    <div class="col-lg-3 "></div>

                    <div class="col-lg-6 align-self-center">
                        <br>
                        <br>
                        <p class="span-telefono_mas_grande blanco_lp llama_ahora">900 100 162</p>

                        <form method="post" action="php/send_mail.php" id="form3" class="contacto" enctype="multipart/form-data" accept-charset="utf-8" style="padding-left:23%; padding-right:23%">
                            <input type="text" name="nombre" id="nombre_3" class="text_box_b" placeholder="nombre">
                            <select name="perfil" class="text_box_b" style="margin-top:20px">
                              <option value="0">Perfil profesional:</option>
                              <option value="particular">Particular</option>
                              <option value="sector">Profesional del sector</option>
                              <option value="despacho">Despacho profesional</option>
                            </select>
                            <br>
                            <input type="tel" name="tel" id="tel_3" class="text_box_b_2" placeholder="teléfono*">
                            <input type="email" name="email" id="email_3" class="text_box_b_3" placeholder="email">
                            <input type="hidden" name="pixel" id="pixel" value="<?= (isset($_GET['gclid']))? 'si' : 'no' ?>" style="display:none;" />

                            <br><br>
                            <span class="btn-enviar_form_2 hvr-ripple-out" id="send_form_3">enviar formulario</span>
                            <br><br>
                            <center>
                                <div class="checkbox">
                                    <label class="checkbox">
                                <input type="checkbox" id="acepto_3"><span class="blanco"
                                                                                   style="position:relative;float: inherit;vertical-align: middle;margin-top:-3px;left:-26px "><i
                                    class="cr-icon fa fa-check"></i></span><span
                                    class="mini_text_1 espaciado-0 blanco_lp" style="margin-left:-20px">  Acepto haber leído los <a
                                    target="_blank" href="https://www.ayudatpymes.com/terminos_y_condiciones" style="color:#fff"><b>Términos y Condiciones</b></a>, <a
                                    target="_blank" href="https://www.ayudatpymes.com/politica_de_privacidad" style="color:#fff"><b>Política de Privacidad</b></a> y <br><a
                                    target="_blank" href="https://www.ayudatpymes.com/acuerdo_de_procesamiento_de_datos" style="color:#fff"><b>Procesamientos de datos</b></a></span>
                            </label>
                                </div>
                            </center>
                        </form>
                    </div>
                    <div class="col-lg-3"></div>
                </div>
            </div>
        </div>




        <!--row 9 logo-->
        <!-- -------------- -------------- -------------- -------------- -------------- -------------- -------------- -------------- -->
        <div class="container-fluid hidden-xs">
            <div class="franja_gris"></div>
            <br><br><br><br>
        </div>

        <div class="container-fluid hidden-lg">
            <div class="franja_gris_xs"></div>
            <br><br>
        </div>

        <div class="container">
            <div class="col-lg-12 text-center margen-up-50">
                <br><br><br><br><br><br><br><br>
                <center>
                    <img src="Img/ayudate_pymes_logo_2x.png" class="img-responsive2" alt="AyudaT Pymes">
                </center>
                <br><br>
            </div>
        </div>


        <div class="container max-1200 lideres text-center hidden-xs hidden-lg hidden-sm">
            <a href="https://www.ayudatpymes.com" target="_blank" class="link2">
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <br><br>
                        <p class="semi-bold-40 espaciado-2 blanco_lp">Asesoría líder para Pymes y autónomos</p>

                        <p class="bold-35 espaciado-3 blanco_lp margen-up-10">GESTIÓN Y ASESORAMIENTO desde 9,99€/mes*</p>
                        <p class="semi-bold-24 espaciado-1  blanco_lp margen-up-10">SOLUCIONES INTEGRALES ADAPTADAS A LAS NECESIDADES DE TU NEGOCIO
                        </p>
                        <p class="bold-36 espaciado-1  blanco_lp margen-up-10">FISCAL, LABORAL, CONTABLE Y JURÍDICO</p>
                    </div>
                </div>
            </a>
            <br><br><br><br><br><br>
            <a href="https://www.ayudatpymes.com" target="_blank" class="links espaciado-2">ayudatpymes.com</a>

        </div>


        <div class="row empresas justify-content-md-center">
         <a href="https://www.ayudatpymes.com"><div class="item-empresas item-empresas1"></div></a>
         <a href="https://ayudatlegal.com/"><div class="item-empresas item-empresas2"></div></a>
         <a href="https://www.ayudatpymes.com/para-despachos/" target="_blank"><div class="item-empresas item-empresas3"></div></a>
         <a href="http://ayudatlearning.com/" target="_blank"><div class="item-empresas item-empresas4"></div></a>
         <a href="https://www.ayudatpymes.com/programa-facturacion-gratuito/" target="_blank"><div class="item-empresas item-empresas5"></div></a>
         <a href="http://liquidoo.es/" target="_blank"><div class="item-empresas item-empresas6"></div></a>
         <a href="https://www.ayudatpymes.com/para-despachos/software-asesorias/" target="_blank"><div class="item-empresas item-empresas7"></div></a>
         <a href="http://ayudat.es/alma" target="_blank"><div class="item-empresas item-empresas8"></div></a>

    </div>

    <footer>
    <div class="logo-footer"><img src="Img/logo-footer.jpg" alt="AyudaT Pymes"/></div>
    <div class="row footer">

        <div class="col-sm-12 col-lg-7">
            <div class="col-sm-4 col-lg-4 web-map">
                <a href="/">Inicio</a>
                <a href="servicios">Servicios</a>
                <a href="asesoria">Autónomo y empresas</a>
                <a href="alta-autonomos">Alta autónomo</a>
                <a href="crear-empresa">Crear empresa</a>
                <a href="abrir-sucursal">Abrir sucursal</a>
                <a href="emprendedores">Emprendedores</a>
                <a href="empresas-sociales">Empresas sociales</a>
                <a href="http://etlsport.es/" target="_blank">Asesoría para deportistas</a>
            </div>
            <div class="col-sm-4 col-lg-4 web-map">
                <a href="juridica">Asesoría jurídica</a>
                <a href="patentes-y-marcas">Registro de marcas</a>
                <a href="servicio-rgpd">RGPD</a>
                <a href="curso-online">Cursos de formación</a>
                <a href="https://www.ayudatpymes.com/cambio-nombre-coche/" target="_blank">Transferencia de vehículos</a>
                <a href="https://www.ayudatpymes.com/programa-facturacion-gratuito/" target="_blank">Programa facturación Gratis</a>
                <a href="https://www.ayudatpymes.com/para-despachos/" target="_blank">Para despachos</a>
                <a href="http://gestron.es/" target="_blank">Blog GesTron</a>
                <a href="https://www.ayudatpymes.com/para-despachos/software-asesorias/" target="_blank">Software asesoría</a>

            </div>
            <div class="col-sm-3 col-lg-3 contact">
                <a href="asesoria-online-ayuda-t-pymes">Conócenos</a>
                <a href="contacto-asesoria-ayuda-t-pymes">Contacto</a>
                <a href="http://www.ayudat.es/" target="_blank">Ayuda T</a>
                <a href="https://mispapeles.es/registro/"><span class="float-right mar-right-16 radj-reg-11 button-nav grey">acceso cliente</span></a>
            </div>
        </div>
        <div class="col-sm-6 col-lg-3">

            <div class="col-sm-12 col-lg-12 contact-img">
                <p>Conectar</p>
                <div class="social">
                    <a href="https://www.facebook.com/AyudaTPymes/" target="_blank"><img src="Img/fb_icon.png" alt="facebook" /></a>
                    <a href="https://twitter.com/AyudaTPymes" target="_blank"><img src="Img/tw_icon.png" alt="tweeter"/></a>
                    <a href="https://www.instagram.com/ayudatpymes/" target="_blank"><img src="Img/ins_icon.png" alt="instagram" /></a>
                    <a href="https://www.linkedin.com/company/ayuda-t-pymes/" target="_blank"><img src="Img/lin_icon.png" alt="linkedin"/></a>
                </div>
                <p>¿Alguna duda laboral, fiscal o contable?</p>
                <a href="http://www.ayudapedia.es/" target="_blank"><img src="Img/ayudapedia.jpg" alt="Ayudapedia" /></a>
            </div>
        </div>
        <div class="col-sm-4 col-lg-2 footer-phone">
            <p class="footer-phone-num phone"><a href="tel:900100162">T. 900 100 162</a></p>
        <p class="footer-phone-txt">Infórmate sin compromiso</p>

        <p class="footer-phone-txt2">Atención al cliente</p>
            <p class="footer-phone-num2 phone"><a href="tel:856500776">T. 856 500 776</a></p>
        <p class="footer-phone-txt3">clientes@ayudatpymes.com</p>

        </div>
    </div>
    <div class="row legal justify-content-md-center">
        <img src="Img/logo-footer-legal.jpg" alt="AyudaT"/>
        <p class="text-legal1">Ayuda-T un lugar todas las Soluciones S.L. AYUDA-T PYMES © 2017 | <a href="terminos_y_condiciones" target="_blank">Términos y condiciones</a> | <a href="politica_de_privacidad" target="_blank">Política de privacidad</a> | <br class="visible-md"><a href="acuerdo_de_procesamiento_de_datos" target="_blank">Procesamiento de datos</a></p>
        <p class="condiciones-legal">*Los precios que aparecen en la web marcados con * no incluyen IVA</p>
    </div>
</footer>


        <!--//BLOQUE COOKIES-->
        <div id="barraaceptacion" style="display: none;">
            <div class="inner">
                Solicitamos su permiso para obtener datos estadísticos de su navegación en esta web, en cumplimiento del Real Decreto-ley 13/2012. Si continúa navegando consideramos que acepta el uso de cookies.
                <a href="javascript:void(0);" class="ok" onclick="PonerCookie();"><b>OK</b></a> |
                <a href="https://www.ayudatpymes.com/servicios-rgpd" target="_blank" class="info">Más información</a>
            </div>
        </div>

        <?php
    $pixel='0'; $envio = 0; $gracias='0';

    if(isset($_GET['r'])) {
        if($_GET['r']=='pixel'){
            $pixel='1'; $envio = 0; $gracias='0';
        } else {
            $gracias = '1';$pixel='0'; $envio = 0;
        }
    }
?>

            <!--//FIN BLOQUE COOKIES-->

            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>


            <!-- Latest compiled and minified JavaScript -->
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>


            <!-- Para las ALERTAS -->
            <script>
                $(document).ready(function() {
                    $('.open').on('click', function() {
                        $('#popup').fadeIn('slow');
                        $('.popup-overlay').fadeIn('slow');
                        $('.popup-overlay').height($(window).height());
                        return false;
                    });

                    $('.popup-overlay').on('click', function() {
                        $('#popup').fadeOut('slow');
                        $('.popup-overlay').fadeOut('slow');
                        return false;
                    });
                });

            </script>
            <div id="popup" style="display: none;">
                <div class="content-popup">

                  <div id="formulario" class="formulario text-center">
                  <p class="text_1 margen-up-10" style="display: block;">​​¿Quieres <span class="rojo_lp">+</span> info?</p>
                  <p class="span-telefono_grande" style="display: block;">​​Deja tus datos</p>

                  <p class="mini_text_1 llama_ahora" style="display: block;">rellena este formulario y te informaremos de todo.</p>
                  <form method="post" action="php/send_mail.php" id="form1" class="contacto" enctype="multipart/form-data" accept-charset="utf-8">
                      <input type="text" name="nombre" id="nombre_1" class="text_box" placeholder="nombre">
                      <select name="perfil" class="text_box" style="margin-top:10px">
                        <option value="0">Perfil profesional:</option>
                        <option value="particular">Particular</option>
                        <option value="sector">Profesional del sector</option>
                        <option value="despacho">Despacho profesional</option>
                      </select>
                      <input type="tel" name="tel" id="tel_1" class="text_box_2" placeholder="teléfono*">
                      <input type="email" name="email" id="email_1" class="text_box_2" placeholder="email">
                      <input type="hidden" name="pixel" id="pixel" value="no" style="display:none;">

                      <br><br>
                      <span class="btn-enviar_form hvr-ripple-out-2" id="send_form_1">enviar formulario</span>
                      <br>
                      <div class="checkbox">
                          <label class="checkbox">
                      <input type="checkbox" id="acepto_1"><span class="cr" style="vertical-align: middle; margin-left:-15px;margin-top:6px"><i class="cr-icon fa fa-check"></i></span>
                      <div class="mini_text_1 espaciado-0 gris_lp" style="margin-top:8px">Acepto haber leído los <a target="_blank" href="https://www.ayudatpymes.com/terminos_y_condiciones"><b>Términos y Condiciones</b></a>,  <br><a target="_blank" href="https://www.ayudatpymes.com/politica_de_privacidad"><b>Política de Privacidad</b></a> y <a target="_blank" href="https://www.ayudatpymes.com/acuerdo_de_procesamiento_de_datos"><b>Procesamientos de datos</b></a></div>
                  </label>
                      </div>

                  </form>
              </div>

                    </div>
                </div>
            </div>
            <div class="popup-overlay"></div>
            <script src="js/sweetalert.min.js"></script>

            <script>
                var pixel = '<?php echo $pixel; ?>';
                var envio = '<?php echo $envio; ?>';
                var gracias = '<?php echo $gracias; ?>';

                function ppcconversion() {

                    if (envio != '1') {
                        var iframe = document.createElement('iframe');
                        iframe.style.width = '0px';
                        iframe.style.height = '0px';
                        document.body.appendChild(iframe);
                        iframe.src = 'codigos_pixeles.php';
                    }
                }

                setTimeout(function() {
                    document.getElementById("barraaceptacion").style.display = "none";
                }, 7000);


                if (gracias == '1') {
                    swal({
                        title: "GRACIAS",
                        text: "Hemos recibido tu solicitud, pronto nos pondremos en contacto",
                        icon: "success",
                        button: " Enterad@ "
                    }).then(function() {
                        limpiar();
                    });
                } else if (pixel == '1') {

                    ppcconversion();
                    ga('send', 'event', 'Formulario', 'Registro', 'Vehiculos');
                    fbq(['track', 'CompleteRegistration']);

                    swal({
                        title: "GRACIAS",
                        text: "Hemos recibido tu solicitud, pronto nos pondremos en contacto",
                        icon: "success",
                        button: " Enterad@ "
                    }).then(function() {
                        envio = 1;
                        limpiar();
                    });
                }


                function getCookie(c_name) {
                    var c_value = document.cookie;
                    var c_start = c_value.indexOf(" " + c_name + "=");
                    if (c_start == -1) {
                        c_start = c_value.indexOf(c_name + "=");
                    }
                    if (c_start == -1) {
                        c_value = null;
                    } else {
                        c_start = c_value.indexOf("=", c_start) + 1;
                        var c_end = c_value.indexOf(";", c_start);
                        if (c_end == -1) {
                            c_end = c_value.length;
                        }
                        c_value = unescape(c_value.substring(c_start, c_end));
                    }
                    return c_value;
                }

                function setCookie(c_name, value, exdays) {
                    var exdate = new Date();
                    exdate.setDate(exdate.getDate() + exdays);
                    var c_value = escape(value) + ((exdays == null) ? "" : "; expires=" + exdate.toUTCString());
                    document.cookie = c_name + "=" + c_value;
                }


                function PonerCookie() {
                    setCookie('avisocookie', '1', 365);
                    document.getElementById("barraaceptacion").style.display = "none";
                }


                $(document).ready(function() {

                    var pictureCount = $('#container img').length;
                    var scrollResolution = 400;


                    var currentdate = new Date();
                    var hora = currentdate.getHours();
                    var minutos = currentdate.getMinutes();

                    currentdate = currentdate.toDateString();
                    currentdate = currentdate.split(' ');
                    var weekday = new Array();

                    if (currentdate[0] == 'Sat' || currentdate[0] == 'Sun' || (hora < 9 || (hora > 20 && minutos > 0))) {
                        $(".fuerahora1").css("display", "block");
                        $(".llama_ahora").css("display", "none");
                    } else {
                        $(".fuerahora1").css("display", "none");
                        $(".llama_ahora").css("display", "block");
                    }

                    if (getCookie('avisocookie') != "1") {
                        document.getElementById("barraaceptacion").style.display = "block";
                    }


                    function animateHorse() {

                        var currentScrollPosition = window.pageYOffset;
                        var imageIndex = Math.round(currentScrollPosition / scrollResolution);

                        if (imageIndex >= pictureCount) {
                            imageIndex = pictureCount - 1; // Select last image
                        }

                        $("#container img").hide();
                        $("#container img").eq(imageIndex).show();
                    }

                    var isElementInView;
                    $(window).bind('scroll', function() {
                        isElementInView = Utils.isElementInView($('#dgt'), false);
                        if (isElementInView) {
                            animateHorse();
                        }

                    });

                    function Utils() {

                    }

                    Utils.prototype = {
                        constructor: Utils,
                        isElementInView: function(element, fullyInView) {
                            var pageTop = $(window).scrollTop();
                            var pageBottom = pageTop + $(window).height();
                            var elementTop = $(element).offset().top;
                            var elementBottom = elementTop + $(element).height();

                            if (fullyInView === true) {
                                return ((pageTop < elementTop) && (pageBottom > elementBottom));
                            } else {
                                return ((elementTop <= pageBottom) && (elementBottom >= pageTop));
                            }
                        }
                    };

                    var Utils = new Utils();

                });


                //animación cara sonriente guiña

                $(".sonrie").bind('mouseover', function() {
                    $(this).attr('src', 'Img/ayudate_pymes_cara_sonriente_guino.png');
                });

                $(".sonrie").bind('mouseout', function() {
                    $(this).attr('src', 'Img/gestoria-cambio-de-nombre-coche.png');
                });

                $(".contacto").bind('submit', function() {

                });


                $("#send_form_1, #send_form_2, #send_form_3, #send_form_4").bind('click', function() {

                    $("#send_form_1, #send_form_2, #send_form_3, #send_form_4").text("Enviando espera...");

                    var acepto = false;

                    switch (this.id) {
                        case "send_form_1":
                            var nombre = $("#nombre_1").val();
                            var tel = $("#tel_1").val();
                            var email = $("#email_1").val();
                            acepto = $("#acepto_1").prop("checked");
                            break;
                        case "send_form_2":
                            var nombre = $("#nombre_2").val();
                            var tel = $("#tel_2").val();
                            var email = $("#email_2").val();
                            acepto = $("#acepto_2").prop("checked");
                            break;
                        case "send_form_3":
                            var nombre = $("#nombre_3").val();
                            var tel = $("#tel_3").val();
                            var email = $("#email_3").val();
                            acepto = $("#acepto_3").prop("checked");
                            break;
                        case "send_form_4":
                            var nombre = $("#nombre_4").val();
                            var tel = $("#tel_4").val();
                            var email = $("#email_4").val();
                            acepto = $("#acepto_4").prop("checked");
                            break;
                    }

                    if (nombre == "" || tel == "" || email == "") {
                        swal({
                            title: "Heeyyyy",
                            text: "Debes completar todos los campos",
                            icon: "error",
                            button: " Comprendido "
                        });
                        $("#send_form_1, #send_form_2, #send_form_3, #send_form_4").text("enviar formulario");
                        return;
                    }

                    if (tel.length < 9) {
                        swal({
                            title: "Heeyyyy",
                            text: "Ese número de teléfono no parece correcto",
                            icon: "error",
                            button: " Comprendido "
                        });
                        $("#send_form_1, #send_form_2, #send_form_3, #send_form_4").text("enviar formulario");
                        return;
                    }

                    if (!email.includes('@') || !email.includes('.')) {
                        swal({
                            title: "Heeyyyy",
                            text: "Ese email no parece correcto",
                            icon: "error",
                            button: " Comprendido "
                        });
                        $("#send_form_1, #send_form_2, #send_form_3, #send_form_4").text("enviar formulario");
                        return;
                    }

                    if (acepto == false) {
                        swal({
                            title: "Heeyyyy",
                            text: "Debes leer y aceptar las condiciones",
                            icon: "error",
                            button: " Comprendido "
                        });
                        $("#send_form_1, #send_form_2, #send_form_3, #send_form_4").text("enviar formulario");
                        return;
                    }

                    switch (this.id) {
                        case "send_form_1":
                            $("#form1").submit();
                            break;
                        case "send_form_2":
                            $("#form2").submit();
                            break;
                        case "send_form_3":
                            $("#form3").submit();
                            break;
                        case "send_form_4":
                            $("#form4").submit();
                            break;
                    }


                    //        mailrelay(email, this.id)
                });


                function limpiar() {
                    $(".form-control").val('');
                    gracias = 0;
                    pixel = 0;
                }

            </script>

            <!-- Google Code para etiquetas de remarketing -->
			<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1038674096;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1038674096/?guid=ON&amp;script=0"/>
</div>
</noscript>
  <?php if(isset($_GET['gracias']) || isset($_GET['pago']) || isset($_GET['r'])){
?>
        <!-- Google Code for Formulario OK Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1038674096;
var google_conversion_language = "es";
var google_conversion_format = "1";
var google_conversion_color = "ffffff";
var google_conversion_label = "74JwCNaUrQQQsNGj7wM";
var google_conversion_value = 1.00;
var google_conversion_currency = "EUR";
var google_remarketing_only = false;
/* ]]> */

</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/1038674096/?value=1.00&amp;currency_code=EUR&amp;label=74JwCNaUrQQQsNGj7wM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

    </div>


<?php } ?>

    </body>

    </html>
