<?php
error_reporting(E_ALL ^ E_NOTICE);
if(!isset($_SESSION))
    session_start();

date_default_timezone_set('Europe/Madrid');
setlocale(LC_CTYPE, 'es_ES.utf8');

require_once(__DIR__.'/php/pasarela/MomoAPICli.php');
require_once(__DIR__.'/php/pasarela/config.php');
require_once(__DIR__.'/vendor/autoload.php');
require_once(__DIR__.'/php/pasarela/fn.php');

$momo = new MomoAPICli(ISS, SUB, CLAVEPRV, CODCOMERCIO, CODTPV, ENPRO);
$mail = new PHPMailer;
$mail->CharSet = 'UTF-8';
// Primera entrada
if (isset($_POST['nombre'])) {

    if (!$_POST['nombre'])
        $error = 'Debes introducir un nombre';

    if (!$_POST['email'])
        $error = 'Debes introducir un email';

    if (!$_POST['tel'])
        $error = 'Debes introducir un teléfono';

    if (!$_POST['matricula'])
            $error = 'Debes introducir una matrícula';

    $_POST['campo1'] = floatval(str_replace(',', '.', str_replace('.', '', $_POST['campo1'])));
    if (!is_numeric($_POST['campo1']))
        $error = 'El importe ha de ser un número.';

    if ($_POST['campo1'] <= 0)
        $error = 'El importe ha de ser un número mayor que cero.';

    if (!$error) {
        $idPago = time().'_'.uniqid();
        $_SESSION['idPago'] = $idPago;
        $_SESSION['fecha'] = date('c', time());
        $_SESSION['campo1'] = $_POST['campo1'];
        $_SESSION['email'] = $_POST['email'];
        $_SESSION['tel'] = $_POST['tel'];
        $_SESSION['nombre'] = $_POST['nombre'];
        $_SESSION['matricula'] = $_POST['matricula'];

        $mail->SetFrom(EMAIL, 'AyudaTpymes');
        if($_SESSION['campo1']!=49.99){
          // mandarMail('juanrosales@ayudat.es','//  Transferencia de Vehículos:','D./Dña.: ' . $_POST['nombre'] . ', con teléfono: '. $_POST['tel'] . ' y correo: '. $_POST['email'].'<br>Ha solicitado información sobre TRANSFERENCIA DE VEHICULOS');
          $cuerpo = '<html xmlns="http://www.w3.org/1999/xhtml">
          <head>
          	<meta http-equiv="content-type" content="text/html; charset=utf-8">
            	<meta name="viewport" content="width=device-width, initial-scale=1.0;">
           	<meta name="format-detection" content="telephone=no"/>

          	<!-- Responsive Mail Template by Konstantin Savchenko, 2015.
          	https://github.com/konsav/email-templates/  -->

          	<style>
          /* Reset styles */
          body { margin: 0; padding: 0; min-width: 100%; width: 100% !important; height: 100% !important;}
          body, table, td, div, p, a { -webkit-font-smoothing: antialiased; text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; line-height: 100%; }
          table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; border-collapse: collapse !important; border-spacing: 0; }
          img { border: 0; line-height: 100%; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; }
          #outlook a { padding: 0; }
          .ReadMsgBody { width: 100%; } .ExternalClass { width: 100%; }
          .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div { line-height: 100%; }

          /* Rounded corners for advanced mail clients only */
          @media all and (min-width: 560px) {
          	.container { border-radius: 8px; -webkit-border-radius: 8px; -moz-border-radius: 8px; -khtml-border-radius: 8px;}
          }

          /* Set color for auto links (addresses, dates, etc.) */
          a, a:hover {
          	color: #127DB3;
          }
          .footer a, .footer a:hover {
          	color: #999999;
          }

           	</style>

          	<!-- MESSAGE SUBJECT -->
          	<title>Transferencia vehículos</title>

          </head>

          <!-- BODY -->
          <!-- Set message background color (twice) and text color (twice) -->
          <body topmargin="0" rightmargin="0" bottommargin="0" leftmargin="0" marginwidth="0" marginheight="0" width="100%" style="border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; width: 100%; height: 100%; -webkit-font-smoothing: antialiased; text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; line-height: 100%;
          	background-color: #F0F0F0;
          	color: #000000;"
          	bgcolor="#F0F0F0"
          	text="#000000">

          <!-- SECTION / BACKGROUND -->
          <!-- Set message background color one again -->
          <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; width: 100%;" class="background"><tr><td align="center" valign="top" style="border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0;"
          	bgcolor="#F0F0F0">

          <!-- WRAPPER -->
          <!-- Set wrapper width (twice) -->
          <table border="0" cellpadding="0" cellspacing="0" align="center"
          	width="560" style="border-collapse: collapse; border-spacing: 0; padding: 0; width: inherit;
          	max-width: 560px;" class="wrapper">

          	<tr>
          		<td align="center" valign="top" style="border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%;
          			padding-top: 20px;
          			padding-bottom: 20px;">

          			<!-- PREHEADER -->
          			<!-- Set text color to background color -->
          			<div style="display: none; visibility: hidden; overflow: hidden; opacity: 0; font-size: 1px; line-height: 1px; height: 0; max-height: 0; max-width: 0;
          			color: #F0F0F0;" class="preheader">
          				</div>

          			<!-- LOGO -->
          			<!-- Image text color should be opposite to background color. Set your url, image src, alt and title. Alt text should fit the image size. Real image size should be x2. URL format: http://domain.com/?utm_source={{Campaign-Source}}&utm_medium=email&utm_content=logo&utm_campaign={{Campaign-Name}} -->
          			<a target="_blank" style="text-decoration: none;"
          				href="http://www.ayudatpymes.com"><img border="0" vspace="0" hspace="0"
          				src="http://www.ayudatpymes.com/cambio-nombre-coche/Img/ayudate_pymes_logo.png"
          				width="100%"
          				alt="Logo" title="Logo" style="
          				color: #000000;
          				font-size: 10px; margin: 0; padding: 0; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; border: none; display: block;" /></a>

          		</td>
          	</tr>

          <!-- End of WRAPPER -->
          </table>

          <!-- WRAPPER / CONTEINER -->
          <!-- Set conteiner background color -->
          <table border="0" cellpadding="0" cellspacing="0" align="center"
          	bgcolor="#FFFFFF"
          	width="560" style="border-collapse: collapse; border-spacing: 0; padding: 0; width: inherit;
          	max-width: 560px;" class="container">

          	<!-- HEADER -->
          	<!-- Set text color and font family ("sans-serif" or "Georgia, serif") -->
          	<tr>
          		<td align="center" valign="top" style="border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%; font-size: 24px; font-weight: bold; line-height: 130%;
          			padding-top: 25px;
          			color: #000000;
          			font-family: sans-serif;" class="header">
          				¡Hey! %nombreCliente%
          				<div class="center"><img src="http://www.ayudatpymes.com/cambio-nombre-coche/Img/ayudate_pymes_volante_rojo_2x.png" style="max-width:70px;margin-top:20px" class="img-responsive volante"></div><br>
          		</td>
          	</tr>

          	<!-- SUBHEADER -->
          	<!-- Set text color and font family ("sans-serif" or "Georgia, serif") -->
          	<tr>
          		<td align="center" valign="top" style="border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-bottom: 3px; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%; font-size: 18px; font-weight: 300; line-height: 150%;
          			padding-top: 5px;
          			color: #000000;
          			font-family: sans-serif;" class="subheader">
          				Tus datos de contacto han sido recibidos correctamente. En breve nos pondremos en contacto contigo y poco después, el trámite de la transferencia de tu vehículo estará gestionado.
          		</td>
          	</tr>

          	<!-- BIG IMAGE -->
          	<!-- Image text color should be opposite to background color. Set your url, image src, alt and title. Alt text should fit the image size. Real image size should be x2 (wrapper x2). Do not set height for flexible images (including "auto"). URL format: http://domain.com/?utm_source={{Campaign-Source}}&utm_medium=email&utm_content={{Ìmage-Name}}&utm_campaign={{Campaign-Name}} -->
          	<!-- <tr>
          		<td align="center" valign="top" style="border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0;
          			padding-top: 20px;" class="hero"><a target="_blank" style="text-decoration: none;"
          			href="http://www.ayudatpymes.com/"><img border="0" vspace="0" hspace="0"
          			src="https://raw.githubusercontent.com/konsav/email-templates/master/images/hero-wide.png"
          			alt="Please enable images to view this content" title="Ayuda-T Image"
          			width="560" style="
          			width: 100%;
          			max-width: 560px;
          			color: #000000; font-size: 13px; margin: 0; padding: 0; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; border: none; display: block;"/></a></td>
          	</tr> -->

          	<!-- PARAGRAPH -->
          	<!-- Set text color and font family ("sans-serif" or "Georgia, serif"). Duplicate all text styles in links, including line-height -->
          	<tr>
          		<td align="center" valign="top" style="border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%; font-size: 17px; font-weight: 400; line-height: 160%;
          			padding-top: 25px;
          			color: #000000;
          			font-family: sans-serif;" class="paragraph">
          				Si fuera necesario, podrás comentarnos cualquier detalle que creas oportuno y ayudaremos a solventarlo, sin problema.
          		</td>
          	</tr>

          	<tr>
          		<td align="center" valign="top" style="border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%; font-size: 17px; font-weight: 400; line-height: 160%;
          			padding-top: 25px;
          			color: #000000;
          			font-family: sans-serif;" class="paragraph">
          				Muchas gracias por tu interés, esperamos que quedes muy satisfecho con nuestra atención.
          		</td>
          	</tr>	<tr>
          		<td align="center" valign="top" style="border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%; font-size: 17px; font-weight: 400; line-height: 160%;
          			padding-top: 25px;
          			color: #000000;
          			font-family: sans-serif; padding-bottom: 25px;" class="paragraph">
          				Saludos y gracias por confiar en Ayuda-T Pymes
          		</td>
          	</tr>






          </table>


          <table border="0" cellpadding="0" cellspacing="0" align="center"
          	width="560" style="border-collapse: collapse; border-spacing: 0; padding: 0; width: inherit;
          	max-width: 560px;" class="wrapper">

          	<tr>
          		<td align="center" valign="top" style="border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%;
          			padding-top: 25px;" class="social-icons"><table
          			width="256" border="0" cellpadding="0" cellspacing="0" align="center" style="border-collapse: collapse; border-spacing: 0; padding: 0;">
          			<tr>

          				<td align="center" valign="middle" style="margin: 0; padding: 0; padding-left: 10px; padding-right: 10px; border-collapse: collapse; border-spacing: 0;"><a target="_blank"
          					href="https://www.facebook.com/pages/Ayuda-T-Pymes-Asesoria-y-Servicios-Empresas/155419431176205"
          				style="text-decoration: none;"><img border="0" vspace="0" hspace="0" style="padding: 0; margin: 0; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; border: none; display: inline-block;
          					color: #000000;"
          					alt="F" title="Facebook"
          					width="44" height="44"
          					src="https://raw.githubusercontent.com/konsav/email-templates/master/images/social-icons/facebook.png"></a></td>

          				<td align="center" valign="middle" style="margin: 0; padding: 0; padding-left: 10px; padding-right: 10px; border-collapse: collapse; border-spacing: 0;"><a target="_blank"
          					href="https://twitter.com/AyudaTPymes"
          				style="text-decoration: none;"><img border="0" vspace="0" hspace="0" style="padding: 0; margin: 0; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; border: none; display: inline-block;
          					color: #000000;"
          					alt="T" title="Twitter"
          					width="44" height="44"
          					src="https://raw.githubusercontent.com/konsav/email-templates/master/images/social-icons/twitter.png"></a></td>

          				<td align="center" valign="middle" style="margin: 0; padding: 0; padding-left: 10px; padding-right: 10px; border-collapse: collapse; border-spacing: 0;"><a target="_blank"
          					href="https://plus.google.com/+Ayudatpymes"
          				style="text-decoration: none;"><img border="0" vspace="0" hspace="0" style="padding: 0; margin: 0; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; border: none; display: inline-block;
          					color: #000000;"
          					alt="G" title="Google Plus"
          					width="44" height="44"
          					src="https://raw.githubusercontent.com/konsav/email-templates/master/images/social-icons/googleplus.png"></a></td>
          			</tr>
          			</table>
          		</td>
          	</tr>

          	<!-- Set text color and font family ("sans-serif" or "Georgia, serif"). Duplicate all text styles in links, including line-height -->
          	<tr>
          		<td align="center" valign="top" style="border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%; font-size: 13px; font-weight: 400; line-height: 150%;
          			padding-top: 20px;
          			padding-bottom: 20px;
          			color: #999999;
          			font-family: sans-serif;" class="footer">
          				Documento generado gracias a Ayuda-T Pymes. Si quieres ver muchas cosas más, GRATIS y para siempre, entra <a href="http://www.ayudatpymes.com/" target="_blank" style="text-decoration: underline; color: #999999; font-family: sans-serif; font-size: 13px; font-weight: 400; line-height: 150%;">aquí</a>.

          		</td>
          	</tr>
          <!-- End of WRAPPER -->
          </table>
          <!-- End of SECTION / BACKGROUND -->
          </td></tr></table>
          </body>
          </html>';
          $cuerpo = str_replace('%nombreCliente%', $_SESSION['nombre'], $cuerpo);
          // mandarMail2($_POST['email'],'Transferencia de Vehículos AyudaTPymes',$cuerpo);



              $CorreoPagina   = 'noreply@'.$_SERVER['SERVER_NAME'];
              $titulo         = '=?UTF-8?B?'.base64_encode($titulo).'?=';                         // Titulo coja tildes y ñ

              $fromaddress = '"Página Web" <'.$CorreoPagina.'>';

              $cabeceras = 'MIME-Version: 1.0'."\r\n".
                  'Content-type: text/html; charset=utf-8'."\r\n".                       // Tipo de texto en el correo.
                  'From: '.$fromaddress."\r\n".                                          // La direccion de correo desde donde supuestamente se envió
                  'Reply-To: '.$fromaddress."\r\n".                                      // La direccion de correo a donde se responderá (cuando el recepto haga click en RESPONDER)
                  'Return-Path: '.$fromaddress."\r\n".                                   // responder a...
                  'Message-ID: <'.time().'-'.$CorreoPagina.'>'."\r\n".                   // anti-spam
                  'X-Mailer: PHP/'.phpversion()."\r\n";                                  // información sobre el sistema de envio de correos, en este caso la version de PHP

              ini_set('sendmail_from',$fromaddress); // anti-spam


                if($_SESSION['campo1']==34.99){
                  $para          = 'solicitud@bidoq.es';
                }else if($_SESSION['campo1']==24.99){
                  $para          = 'vehiculos@ayudatpymes.com';
                }                                               // A quien enviamos el correo.
              $para          = 'solicitud@ayudatpymes.com';        // Metemos para quienes va a ir.
              $titulo        = 'Transferencia de Vehículos: ';                      // Meto en el titulo desde que servidor ha sido enviado.
              $mensaje       = 'D./Dña.: ' . $_POST['nombre'] . ', con teléfono: '. $_POST['tel'] . ' y correo: '. $_POST['email'].'<br>Ha solicitado información sobre TRANSFERENCIA DE VEHICULOS'; // Ponemos al final del mensaje a quien iba.




               $CorreoPagina2   = 'noreply@'.$_SERVER['SERVER_NAME'];
               $titulo2         = '=?UTF-8?B?'.base64_encode($titulo).'?=';                         // Titulo coja tildes y ñ

               $fromaddress2 = '"Página Web" <'.$CorreoPagina2.'>';

               $cabeceras2 = 'MIME-Version: 1.0'."\r\n".
                   'Content-type: text/html; charset=utf-8'."\r\n".                       // Tipo de texto en el correo.
                   'From: '.$fromaddress2."\r\n".                                          // La direccion de correo desde donde supuestamente se envió
                   'Reply-To: '.$fromaddress2."\r\n".                                      // La direccion de correo a donde se responderá (cuando el recepto haga click en RESPONDER)
                   'Return-Path: '.$fromaddress2."\r\n".                                   // responder a...
                   'Message-ID: <'.time().'-'.$CorreoPagina2.'>'."\r\n".                   // anti-spam
                   'X-Mailer: PHP/'.phpversion()."\r\n";                                  // información sobre el sistema de envio de correos, en este caso la version de PHP

               ini_set('sendmail_from',$fromaddress2); // anti-spam


               $para2          = $_POST['email'];        // Metemos para quienes va a ir.
               $titulo2        = 'Transferencia de Vehículos: ';                                         // A quien enviamos el correo.
                    // Metemos para quienes va a ir.
                                   // Meto en el titulo desde que servidor ha sido enviado.
                // Ponemos al final del mensaje a quien iba.

               if ( mail($para2, $titulo2, $cuerpo, $cabeceras2 ) &&  mail($para, $titulo, $mensaje, $cabeceras )){
                  header('Location:contratar?gracias=ok');
               } else {
                   ini_restore('sendmail_from');
                   return FALSE;
               }




        }else {
        // Mail antes de solicitar el pago
        $CorreoPagina   = 'noreply@'.$_SERVER['SERVER_NAME'];
        $titulo         = '=?UTF-8?B?'.base64_encode($titulo).'?=';                         // Titulo coja tildes y ñ

        $fromaddress = '"Página Web" <'.$CorreoPagina.'>';

        $cabeceras = 'MIME-Version: 1.0'."\r\n".
            'Content-type: text/html; charset=utf-8'."\r\n".                       // Tipo de texto en el correo.
            'From: '.$fromaddress."\r\n".                                          // La direccion de correo desde donde supuestamente se envió
            'Reply-To: '.$fromaddress."\r\n".                                      // La direccion de correo a donde se responderá (cuando el recepto haga click en RESPONDER)
            'Return-Path: '.$fromaddress."\r\n".                                   // responder a...
            'Message-ID: <'.time().'-'.$CorreoPagina.'>'."\r\n".                   // anti-spam
            'X-Mailer: PHP/'.phpversion()."\r\n";                                  // información sobre el sistema de envio de correos, en este caso la version de PHP

        ini_set('sendmail_from',$fromaddress); // anti-spam


                                                                 // A quien enviamos el correo.
        $para          = 'solicitud@ayudatpymes.com';        // Metemos para quienes va a ir.
        $titulo        = 'Transferencia de Vehículos: ';                      // Meto en el titulo desde que servidor ha sido enviado.
        $mensaje       = 'D./Dña.: ' . $_POST['nombre'] . ', con teléfono: '. $_POST['tel'] . ' y correo: '. $_POST['email'].'<br>Ha solicitado información sobre TRANSFERENCIA DE VEHICULOS'; // Ponemos al final del mensaje a quien iba.

        if ( mail($para, $titulo, $mensaje, $cabeceras )){
          header('Location: '.$momo->inicioPagoWeb(
                  $_SESSION['campo1'],
                  $_SESSION['fecha'],
                  $idPago,
                  URL_OK,
                  URL_KO,
                  $_SESSION['nombre'], //nombre completo
                  $_SESSION['email'],
                  null,
                  [ ['concepto' => 'Transferencia vehículos: '.$_SESSION['nombre'].' - '.$_SESSION['email'], 'importe' => $_SESSION['campo1']] ]
              ));
        } else {
            ini_restore('sendmail_from');
            return FALSE;
        }



    }
}
}
// los datos llegan por url
if (('ok' == $_GET['pago'] || 'ko' == $_GET['pago']) && isset($_SESSION['fecha'])) {
    $datosComprueba = ['codOperacionExterno' => 'Ninguno', 'codigoRetorno' => 'Ninguno'];

    try {
        $datosComprueba = $momo->comprobarPagoWeb($_SESSION['fecha'], $_POST['uuidRetorno']);
    } catch (Exception $e) {}

    if ('ko' != $_GET['pago']) {
        $mail->SetFrom(EMAIL, 'AyudaTpymes');

        $mail->addAddress(EMAILINFO);
        //foreach (explode(',', EMAILINFO) as $to) $mail->addAddress(trim($to));

        $captionResultado = ('0502' == $datosComprueba['codigoRetorno'] ? 'Pago realizado' : 'Error en pago o pago cancelado');

        $mail->Subject = 'AyudaTpymes - '.$captionResultado;

        $codigosRetorno = [
            '0501' => 'La operación no existe',
            '0502' => 'Pago realizado con éxito',
            '0503' => 'Operación finalizada pero con algún error',
            '0504' => 'Operación no finalizada',
        ];

        $datos = [
            'Fecha: '.date('d/m/Y H:i:s', strtotime($_SESSION['fecha'])),
            'Importe: '.$_SESSION['campo1'].' EUR',
            'Email: '.$_SESSION['email'],
            'Teléfono:'.$_SESSION['tel'],
            'Matrícula:'.$_SESSION['matricula'],
            'Código de operación externo: '.$datosComprueba['codOperacionExterno'],
            'Código de retorno: '.$datosComprueba['codigoRetorno'].' '.$codigosRetorno[$datosComprueba['codigoRetorno']],
        ];

        $mail->Body = implode(" \r\n", $datos);

        logResultado(array_merge([$captionResultado], $datos));

        //if (!$mail->send()) { echo $mail->ErrorInfo; die(); }
        if ($mail->send()) {
            // Copia MAIL cliente
            $mail->IsHTML(true);
            $mail->SetFrom(EMAIL, 'AyudaTpymes');
            $mail->ClearAllRecipients();
            $mail->addAddress($_SESSION['email']);
            $captionResultado = 'Pago realizado';
            $mail->Subject = 'AyudaTpymes - '.$captionResultado;
            $message = file_get_contents('./mailCliente.html');
            $message = str_replace('%nombreCliente%', $_SESSION['nombre'], $message);
            $mail->MsgHTML($message);
            $mail->send();
        }


        unset($_SESSION['fecha']);
        header('Location:contratar?pago=ok');
    }



}

?>
<!DOCTYPE html>
<html lang="es">

<head>
    <!-- Required meta tags -->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="application-name" content="Cambio de nombre coche" />
    <meta name="author" content="AyudaT Pymes" />
    <meta name="title" content="Cambio de nombre coche">
    <meta name="dcterms.title" content="Cambio de nombre coche" />
    <meta name="description" content="Transferencia vehículos: todo hecho, el mejor precio y servicio experto, sin que tú te muevas de casa" />
    <meta name="robots" content="index, follow" />
    <title>Cambio de nombre coche</title>

    <!-- Bootstrap CSS -->
    <!-- Latest compiled and minified CSS -->
    <link rel="icon" href="favicon.ico" type="image/x-icon"/>
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon"/>

    <!-- MODALES -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/izimodal/1.5.1/css/iziModal.css">

    <!-- CSS LOCALES -->

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
     <script src="js/modernizr.custom.34982.js"></script>
    <script src="js/sketcher.js"></script>
    <script src="js/trigonometry.js"></script>
    <!-- FUENTES -->
    <link href="https://fonts.googleapis.com/css?family=Rajdhani:300,400,500,600,700" rel="stylesheet">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/css.css">
    <link rel="stylesheet" href="css/animaciones.css">
    <!-- ======================================================================= -->
      <!-- ===============================PIXELES================================= -->
      <!-- ======================================================================= -->

      <!-- Facebook Pixel Code -->
      <?php if(isset($_GET['gracias']) || isset($_GET['pago'])){
  ?>
  	<script>
  	!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  	n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
  	n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
  	t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
  	document,'script','https://connect.facebook.net/en_US/fbevents.js');

  	fbq('init', '660233234086506');
  	fbq('track', 'PageView');
      fbq('track', 'CompleteRegistration');
  	</script>
      <noscript><img height='1' width='1' style='display:none'
  	src='https://www.facebook.com/tr?id=660233234086506&ev=PageView&noscript=1'
  	/></noscript>
  	    <script data-obct type="text/javascript">
  /* DO NOT MODIFY THIS CODE */
  !function(_window, _document) {
  var OB_ADV_ID='004195809e89cb3dd64a935b5724ee9f17';
  if (_window.obApi) {var toArray = function(object) {return Object.prototype.toString.call(object) === '[object Array]' ? object : [object];};_window.obApi.marketerId = toArray(_window.obApi.marketerId).concat(toArray(OB_ADV_ID));return;}
  var api = _window.obApi = function() {api.dispatch ? api.dispatch.apply(api, arguments) : api.queue.push(arguments);};api.version = '1.1';api.loaded = true;api.marketerId = OB_ADV_ID;api.queue = [];var tag = _document.createElement('script');
  tag.async = true;tag.src = '//amplify.outbrain.com/cp/obtp.js';tag.type = 'text/javascript';var script = _document.getElementsByTagName('script')[0];script.parentNode.insertBefore(tag, script);}(window, document);
  obApi('track', 'PAGE_VIEW');
  </script>
      <?php } else{?>

      <script>
  	!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  	n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
  	n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
  	t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
  	document,'script','https://connect.facebook.net/en_US/fbevents.js');

  	fbq('init', '660233234086506');
  	fbq('track', 'PageView');
  	</script>
  	<noscript><img height='1' width='1' style='display:none'
  	src='https://www.facebook.com/tr?id=660233234086506&ev=PageView&noscript=1'
  	/></noscript>
  	<!-- End Facebook Pixel Code -->
  <?php } ?>

      <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-8047470-2"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-8047470-2');
  </script>


    <style type="text/css">
        .error { padding-bottom: 15px; font-weight: bold; }
    </style>

</head>

<body>

    <div class="container-fluid">
        <div class="linea_gris"></div>
    </div>
    <div class="container-fluid max-1200">

        <!--row 1 menu superior-->
        <!-- -------------- -------------- -------------- -------------- -------------- -------------- -------------- -------------- -->

        <!--Medias Large-->

        <nav class="navbar navbar-light light-blue lighten-4">

<!-- Navbar brand -->
<a class="navbar-brand" href="https://www.ayudatpymes.com/cambio-nombre-coche">  <img src="Img/ayudate_pymes_logo.png" class="img-responsive" alt="Ayuda T Pymes">
<span class="breadcrumbs"><span> | </span>TRANSFERENCIA VEHÍCULO</span></a>
<!-- Collapse button -->
<div class="content-nav">




    <a href="contratar">
<span class="btn-contratar sacudir hvr-ripple-out-2-rojo contratar-nav">CONTRATA!</span>
</a>



    <a href="http://www.ayudat.es" target="_blank">
<img src="Img/ayudat_pymes_icono_ayudat.png" alt="Ayuda T">
</a>

</div>



<!-- Collapsible content -->

<!-- Collapsible content -->

</nav>
<div class="row justify-content-around menu">
  <div class="link1"><a class="nav-link" href="contrato-compraventa-vehiculo">Generador de contratos</a></div>
  <div class="link1"><a class="nav-link" href="calcular-transferencia-coche">Calculadora de transferencia</a></div>
</div>


        <!--row 2 Franja roja con volante-->
        <!-- -------------- -------------- -------------- -------------- -------------- -------------- -------------- -------------- -->

        <!--Medias PCS-->


        <div class="row faldon_1 ">

            <div class="col-lg-12 col-md-12 text-center">
                <center>
                    <h1 class="titulo ">Contratar transferencia de vehículo online</h1>
                    <img src="Img/precio-cambio-de-nombre-en-gestoria.png" class="img-responsive volante" alt="Precio cambio de nombre en gestoría">
                </center>
            </div>
        </div>
</div>


        <div class="container-fluid max-1200">
            <div class="row justify-content-center">
              <div class="col-lg-10">
          <?php if(isset($_GET['gracias']) || isset($_GET['pago'])){

                  if($_GET['gracias']='ok'){?>
                  <p class="semi-bold-25 linea_media rojo_lp" style="text-align:center; margin-top: 50px;">¡Gracias por confiar en nosotros!</p>
                  <br><br>
                  <p class="bold-25 linea_media rojo_lp">En breve recibirá la llamada de uno de nuestros especialistas para continuar con el trámite.</p>
                  <p class="bold-25 linea_media rojo_lp">¡Un Saludo!</p>
                  <br>
                  <br>
                  <br>
                  <img src="Img/ayudate_pymes_volante.png">
                  <br><br>
                <?php }else if ($_GET['pago']='ok'){?>
                <p class="semi-bold-25 linea_media rojo_lp">¡Gracias por confiar en nosotros!</p>
                <br><br>
                <p class="bold-25 linea_media rojo_lp">Te hemos enviado un correo con información importante</p>
                <p class="bold-25 linea_media rojo_lp">sobre la documentación necesaria a aportar, en breve</p>
                <p class="bold-25 linea_media rojo_lp">nuestros técnicos le contactarán telefónicamente.</p>
                <br>
                <br>
                <br>
                <img src="Img/ayudate_pymes_volante.png">
                <br><br>
        <?php
}
}else{
 ?>
          <form method="post" action="<?php echo parse_url($_SERVER["REQUEST_URI"], PHP_URL_PATH); ?>" id="form2" class="contacto" enctype="multipart/form-data" accept-charset="utf-8">
            <div class="row justify-content-center" id="contrata-1">
                <div class="col-lg-12">
                  <h2>Necesitamos conocer tu perfil antes de empezar con el trámite:</h2>
                  <div class="row justify-content-center">
                    <div class="col-lg-3 item1">
                    <label class="labl">
                        <input type="radio" name="campo1"  value="49,99" id="particular"/>
                        <div><h3>Particular</h3>
                          <div class="row justify-content-center precio">
                            <div class="col-lg-9">
                            <p class="pastilla">49<sup>,99*</sup> €</p>
                          </div>
                        </div>
                          </div>
                    </label>
                    </div>
                    <div class="col-lg-3 item1">
                  <label class="labl">
    <input type="radio" name="campo1"  value="24,99" />
    <div><h3>Profesional del sector</h3>
      <div class="row justify-content-center">
        <div class="col-lg-9 submenu">
          <p>- Concesionario</br>
          - Renting</br>
        - Compra/venta</p>
        </div>
      </div>
      <div class="row justify-content-center precio">
        <div class="col-lg-9">
        <p class="pastilla">24<sup>,99*</sup> €</p>
      </div>
      </div>
      </div>
</label>
</div>
<div class="col-lg-3 item1">
<label class="labl">
    <input type="radio" name="campo1"  value="34,99" />
    <div><h3>Despacho profesional</h3>
      <div class="row justify-content-center">
        <div class="col-lg-6 submenu">
      <p>- Asesoría</br>
      - Gestoría</p>
    </div>
  </div>
  <div class="row justify-content-center precio">
    <div class="col-lg-9">
    <p class="pastilla">34<sup>,99*</sup> €</p>
  </div>
</div>
  </div>
</label>
</div>

</div>

</div>
</div>
    <div class="row justify-content-center" id="comprador" style="display:none">
      <div class="col-lg-12 botones-form">
        <h2>¿Quieres comprar o vender un vehículo?</h2>
        <div class="row justify-content-center">
          <div class="col-lg-4 item2">
            <label class="labl2">
        <input type="radio" name="campo2" value="comprador" />
        <div><p>Comprador</p></div>
        </label>
          </div>
          <div class="col-lg-4 item2">
        <label class="labl2">
        <input type="radio" name="campo2" value="vendedor"/>
        <div><p>Vendedor</p></div>
        </label>
        </div>
      </div>
    </div>
      <div class="row justify-content-center">
        <div class="col-lg-8">
          <div class="boton-contrata botton-left" id="anterior1">ANTERIOR</div>

        </div>
      </div>
</div>


<div class="row justify-content-center" id="persona" style="display:none">
  <div class="col-lg-12 botones-form">
    <h2>¿El vehículo se compra o vende a nombre de una persona física o al de una sociedad?</h2>
    <div class="row justify-content-center">
      <div class="col-lg-4 item3">
            <label class="labl2">
        <input type="radio" name="campo3" value="fisica"/>
        <div><p>Persona física</p></div>
        </label>
      </div>
      <div class="col-lg-4 item3">
        <label class="labl2">
        <input type="radio" name="campo3" value="juridica" />
        <div><p>Persona jurídica</p></div>
        </label>
      </div>
    </div>
  </div>
  <div class="row justify-content-center">
    <div class="col-lg-8">
      <div class="boton-contrata botton-left" id="anterior2">ANTERIOR</div>

    </div>
  </div>
</div>

      <div class="row justify-content-center" id="datos-contratar" style="display:none">
        <div class="col-lg-12 botones-form">
          <h2 id="titulo-enviar">Deja tus datos ahí abajo, te llamo y empezamos cuando quieras:</h2>
          <div class="row justify-content-center">
                <div class="col-lg-4 datos-form">
                  <input type="text" name="nombre" placeholder="nombre completo">

                  <input type="email" name="email" placeholder="email">

                  <input type="tel" name="tel" placeholder="teléfono">

                  <input type="text" name="matricula" placeholder="matrícula">

                  <p>Al pulsar sobre el siguiente botón acepto los términos y condiciones, política de privacidad y procesamiento de datos.*</p>

                  <button type="submit" class="pagar-form" id="enviar0">INFÓRMATE</button>

                </div>
</div>
</div>
<div class="row justify-content-center">
  <div class="col-lg-7">
    <div class="boton-contrata botton-left" id="anterior3">ANTERIOR</div>
  </div>
</div>
            </div>

          </form>
        <?php } ?>
        </div>
</div>
</div>



<div class="row empresas justify-content-md-center">
 <a href="https://www.ayudatpymes.com"><div class="item-empresas item-empresas1"></div></a>
 <a href="https://ayudatlegal.com/"><div class="item-empresas item-empresas2"></div></a>
 <a href="https://www.ayudatpymes.com/para-despachos/" target="_blank"><div class="item-empresas item-empresas3"></div></a>
 <a href="http://ayudatlearning.com/" target="_blank"><div class="item-empresas item-empresas4"></div></a>
 <a href="https://www.ayudatpymes.com/programa-facturacion-gratuito/" target="_blank"><div class="item-empresas item-empresas5"></div></a>
 <a href="http://liquidoo.es/" target="_blank"><div class="item-empresas item-empresas6"></div></a>
 <a href="https://www.ayudatpymes.com/para-despachos/software-asesorias/" target="_blank"><div class="item-empresas item-empresas7"></div></a>
 <a href="http://ayudat.es/alma" target="_blank"><div class="item-empresas item-empresas8"></div></a>

</div>

<footer>
<div class="logo-footer"><img src="Img/logo-footer.jpg" alt="AyudaT Pymes"/></div>
<div class="row footer">

<div class="col-sm-12 col-lg-7">
    <div class="col-sm-4 col-lg-4 web-map">
        <a href="/">Inicio</a>
        <a href="servicios">Servicios</a>
        <a href="asesoria">Autónomo y empresas</a>
        <a href="alta-autonomos">Alta autónomo</a>
        <a href="crear-empresa">Crear empresa</a>
        <a href="abrir-sucursal">Abrir sucursal</a>
        <a href="emprendedores">Emprendedores</a>
        <a href="empresas-sociales">Empresas sociales</a>
        <a href="http://etlsport.es/" target="_blank">Asesoría para deportistas</a>
    </div>
    <div class="col-sm-4 col-lg-4 web-map">
        <a href="juridica">Asesoría jurídica</a>
        <a href="patentes-y-marcas">Registro de marcas</a>
        <a href="servicio-rgpd">RGPD</a>
        <a href="curso-online">Cursos de formación</a>
        <a href="https://www.ayudatpymes.com/cambio-nombre-coche/" target="_blank">Transferencia de vehículos</a>
        <a href="https://www.ayudatpymes.com/programa-facturacion-gratuito/" target="_blank">Programa facturación Gratis</a>
        <a href="https://www.ayudatpymes.com/para-despachos/" target="_blank">Para despachos</a>
        <a href="http://gestron.es/" target="_blank">Blog GesTron</a>
        <a href="https://www.ayudatpymes.com/para-despachos/software-asesorias/" target="_blank">Software asesoría</a>

    </div>
    <div class="col-sm-3 col-lg-3 contact">
        <a href="asesoria-online-ayuda-t-pymes">Conócenos</a>
        <a href="contacto-asesoria-ayuda-t-pymes">Contacto</a>
        <a href="http://www.ayudat.es/" target="_blank">Ayuda T</a>
        <a href="https://mispapeles.es/registro/"><span class="float-right mar-right-16 radj-reg-11 button-nav grey">acceso cliente</span></a>
    </div>
</div>
<div class="col-sm-6 col-lg-3">

    <div class="col-sm-12 col-lg-12 contact-img">
        <p>Conectar</p>
        <div class="social">
            <a href="https://www.facebook.com/AyudaTPymes/" target="_blank"><img src="Img/fb_icon.png" alt="facebook" /></a>
            <a href="https://twitter.com/AyudaTPymes" target="_blank"><img src="Img/tw_icon.png" alt="tweeter"/></a>
            <a href="https://www.instagram.com/ayudatpymes/" target="_blank"><img src="Img/ins_icon.png" alt="instagram" /></a>
            <a href="https://www.linkedin.com/company/ayuda-t-pymes/" target="_blank"><img src="Img/lin_icon.png" alt="linkedin"/></a>
        </div>
        <p>¿Alguna duda laboral, fiscal o contable?</p>
        <a href="http://www.ayudapedia.es/" target="_blank"><img src="Img/ayudapedia.jpg" alt="Ayudapedia" /></a>
    </div>
</div>
<div class="col-sm-4 col-lg-2 footer-phone">
    <p class="footer-phone-num phone"><a href="tel:900100162">T. 900 100 162</a></p>
<p class="footer-phone-txt">Infórmate sin compromiso</p>

<p class="footer-phone-txt2">Atención al cliente</p>
    <p class="footer-phone-num2 phone"><a href="tel:856500776">T. 856 500 776</a></p>
<p class="footer-phone-txt3">clientes@ayudatpymes.com</p>

</div>
</div>
<div class="row legal justify-content-md-center">
<img src="Img/logo-footer-legal.jpg" alt="AyudaT"/>
<p class="text-legal1">Ayuda-T un lugar todas las Soluciones S.L. AYUDA-T PYMES © 2017 | <a href="terminos_y_condiciones" target="_blank">Términos y condiciones</a> | <a href="politica_de_privacidad" target="_blank">Política de privacidad</a> | <br class="visible-md"><a href="acuerdo_de_procesamiento_de_datos" target="_blank">Procesamiento de datos</a></p>
<p class="condiciones-legal">*Los precios que aparecen en la web no incluyen IVA</p>
</div>
</footer>



<!--//BLOQUE COOKIES-->
<div id="barraaceptacion" style="display: none;">
  <div class="inner">
      Solicitamos su permiso para obtener datos estadísticos de su navegación en esta web, en cumplimiento del Real Decreto-ley 13/2012. Si continúa navegando consideramos que acepta el uso de cookies.
      <a href="javascript:void(0);" class="ok" onclick="PonerCookie();"><b>OK</b></a> |
      <a href="https://www.ayudatpymes.com/servicios-rgpd" target="_blank" class="info">Más información</a>
  </div>
</div>


  <!--//FIN BLOQUE COOKIES-->

  <!--Formulario para MailRelay-->


  <!--fin mailrelay-->

  <!-- Latest compiled and minified JavaScript -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
          integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
          crossorigin="anonymous"></script>


  <!-- Moales -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/izimodal/1.5.1/js/iziModal.js"></script>


  <!-- Para las ALERTAS -->

  <script src="js/sweetalert.min.js"></script>
            <script>
                $(document).ready(function () {


                    if(envio != '1' ){

                        var  iframe = document.createElement('iframe');
                        iframe.style.width = '0px';
                        iframe.style.height = '0px';
                        document.body.appendChild(iframe);
                        iframe.src = 'codigos_pixeles.html';
                    }


                    var pictureCount = $('#container img').length;
                    var scrollResolution = 400;

                    var campo1 = "74,95"; // importe que cambiará cuando se seleccione otra opción

                    function animateHorse() {

                        var currentScrollPosition = window.pageYOffset;
                        var imageIndex = Math.round(currentScrollPosition / scrollResolution);

                        if (imageIndex >= pictureCount) {
                            imageIndex = pictureCount - 1; // Select last image
                        }

                        $("#container img").hide();
                        $("#container img").eq(imageIndex).show();
                    }

                    var isElementInView;
                    $(window).bind('scroll', function () {
                        isElementInView = Utils.isElementInView($('#dgt'), false);
                        if (isElementInView) {
                            animateHorse();
                        }

                    });

                    function Utils() {

                    }

                    Utils.prototype = {
                        constructor: Utils,
                        isElementInView: function (element, fullyInView) {
                            var pageTop = $(window).scrollTop();
                            var pageBottom = pageTop + $(window).height();
                            var elementTop = $(element).offset().top;
                            var elementBottom = elementTop + $(element).height();

                            if (fullyInView === true) {
                                return ((pageTop < elementTop) && (pageBottom > elementBottom));
                            } else {
                                return ((elementTop <= pageBottom) && (elementBottom >= pageTop));
                            }
                        }
                    };

                    var Utils = new Utils();


                });


                // controlo que se seleccione una única opcion de compra

                $('body :checkbox').click(function(){
                    var seleccionado = this;
                    if (!seleccionado.checked)
                        return false;

                    importe = this.value;

                    $("input:checked").each(function() {
                        this.checked=false;
                        seleccionado.checked=true;
                    });

                    // Se actualiza importe en form
                    //var auxImporte = parseFloat(importe.replace(',', '.'));
                    $('#importe').val(importe);

                });

                //animación cara sonriente guiña

                $(".sonrie").bind('mouseover', function(){
                    $(this).attr('src','Img/ayudate_pymes_cara_sonriente_guino.png');
                });
                $(".sonrie").bind('mouseout', function(){
                    $(this).attr('src','Img/ayudate_pymes_cara_sonriente_2x.png');
                });

                // proceso de control de datos introducidos correctamente

                $("#enviar, #enviar0").bind('click', function(){
                    var nombre      = $("#nombre").val();

                    var email       = $("#email").val();
                    var telefono    = $("#tel").val();

                    if(nombre == "" || email == "" || telefono == "" ){
                        swal({
                            title: "Heeyyy",
                            text: "Debes introducir todos los datos",
                            icon: "error",
                            button: " Enterad@ ",
                        });
                        return;
                    }
                });
            //
            //    $(".formulario_contrata").bind('submit', function(){
            //        var email = $("#email").val();
            //        $.ajax({
            //            url: 'https://ayudatpymes.ip-zone.com/ccm/subscribe/index/form/g2d8ch5kzy',
            //            data: {'groups[]':'18', 'email':email},
            //            cache: false,
            //            type: 'POST'
            //        }).always(function( data ) {
            //
            //        });
            //    });

                $(".formulario_contrata").bind('submit', function(){
                    var email = $("#email").val();
                    try {
                        ga('send', 'event', 'Formulario', 'Registro', 'Vehiculos');
                        fbq(['track', 'CompleteRegistration']);
                        $("#email-relay").val(email);
                        $("#form-mailrelay").submit();
                    } catch(e){
                        alert(e.message());
                    }
                });


            </script>
            <!-- Google Code para etiquetas de remarketing -->
			<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1038674096;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1038674096/?guid=ON&amp;script=0"/>
</div>
</noscript>
  <?php if(isset($_GET['gracias']) || isset($_GET['pago'])){
?>
        <!-- Google Code for Formulario OK Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1038674096;
var google_conversion_language = "es";
var google_conversion_format = "1";
var google_conversion_color = "ffffff";
var google_conversion_label = "74JwCNaUrQQQsNGj7wM";
var google_conversion_value = 1.00;
var google_conversion_currency = "EUR";
var google_remarketing_only = false;
/* ]]> */

</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/1038674096/?value=1.00&amp;currency_code=EUR&amp;label=74JwCNaUrQQQsNGj7wM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

    </div>


<?php } ?>
            <script type="text/javascript" src="js/modal-formulario.js"></script>
            </body>
            </html>
